/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author ovidio
 */
public class ConsultaAlerta
{
 private Connection conexion;
    
     public ConsultaAlerta(Connection con){
        
        this.conexion = con;
    }
    
    public void mostrarAlerta(){
    try {
            String query = "SELECT * FROM alerta";

            // create the java statement
            Statement st = (Statement) conexion.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombrealerta");
                String descripcion = rs.getString("descripcion");
                // print the results
                System.out.format("%s, %s, %s\n", id, nombre, descripcion);
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! alerta1");
            System.err.println(e.getMessage());
        }
    
    
    }
    
    public void insertarAlerta(int id, String nombre, String descripcion){
    
        try {
            String query = " insert into empresa (id, nombrealerta, descripcion)"
                    + " values (?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setInt(1, id);
            preparedStmt.setString(2, nombre);
            preparedStmt.setString(3, descripcion);
            preparedStmt.execute();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! alerta2");
            System.err.println(e.getMessage());
        }
        
        
    }
    
    public void modificarAlerta(int id, String nombre, String descripcion){
    
        try {
            String query = "update empresa set nombrealerta = ?, descripcion =? where id = ? ";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setInt(1, id);
            preparedStmt.setString(2, nombre);
            preparedStmt.setString(3, descripcion);
            preparedStmt.executeUpdate();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! alerta3");
            System.err.println(e.getMessage());
        }
    }
    
    public void elminarAlerta(int id){
    
         try {
            String query = "delete from empresa where id =?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setInt(1, id);
            preparedStmt.execute();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! alerta4");
            System.err.println(e.getMessage());
        }
    }
    
    public String getNombre(int id){
    try {
            String query = "SELECT * FROM alerta";

            // create the java statement
            Statement st = (Statement) conexion.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);
            
            //String nombre = rs.getString("nombrealerta");
            String nombre ="";
            // iterate through the java resultset
            while (rs.next()) {
                int id2 = rs.getInt("id");
                nombre = rs.getString("nombrealerta");
                String descripcion = rs.getString("descripcion");
                // print the results
                System.out.format("%s, %s, %s\n", id, nombre, descripcion);
                if(id == id2)
                {
                    st.close();
                    return nombre;
                }
            }

            st.close();
            return null;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! alerta5");
            System.err.println(e.getMessage());
        }
    
        return null;
    }
    
    public String getDescricpion(int id){
    try {
            String query = "SELECT * FROM alerta";

            // create the java statement
            Statement st = (Statement) conexion.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);
            
            //String descripcion = rs.getString("descripcion");
            String descripcion = "";
            // iterate through the java resultset
            while (rs.next()) {
                int id2 = rs.getInt("id");
                String nombre = rs.getString("nombrealerta");
                descripcion = rs.getString("descripcion");
                // print the results
                System.out.format("%s, %s, %s\n", id, nombre, descripcion);
                if(id == id2)
                {
                    st.close();
                    return descripcion;
                }
            }

            st.close();
            return null;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! alerta6");
            System.err.println(e.getMessage());
        }
    
        return null;
    
    }
    
}

