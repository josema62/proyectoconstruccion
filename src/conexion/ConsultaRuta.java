/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import ModeloDatos.Ruta;

import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ignacio Correa
 */
public class ConsultaRuta {

    private Connection conexion;
    private ArrayList<Ruta> rutas = new ArrayList<Ruta>();

    public ConsultaRuta(Connection conexion) {
        this.conexion = conexion;
    }

    public ArrayList<Ruta> getRutas() {
        return rutas;
    }

    public void mostrarRuta(String fk_empresa) {
        try {
            String query = "SELECT\n"
                    + "ruta.id,\n"
                    + "ruta.origen,\n"
                    + "ruta.destino,\n"
                    + "ruta.distancia\n"
                    + "FROM\n"
                    + "empleadoadministrador\n"
                    + "INNER JOIN empresa ON empleadoadministrador.fk_empresa = empresa.rut\n"
                    + "INNER JOIN empresatieneruta ON empresatieneruta.fk_empresa = empresa.rut\n"
                    + "INNER JOIN ruta ON empresatieneruta.fk_ruta = ruta.id\n"
                    + "WHERE empresatieneruta.fk_empresa=?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, fk_empresa);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String origen = rs.getString("origen");
                String destino = rs.getString("destino");
                String distancia = rs.getString("distancia");
                System.out.format("%s, %s, %s, %s\n", id, origen, destino, distancia);
                ConsultaParadero paradero = new ConsultaParadero(conexion);
                ConsultaSalida salida = new ConsultaSalida(conexion);
                Ruta ruta = new Ruta(id, origen, destino, distancia, paradero.getParaderos(), salida.getSalidas());
                rutas.add(ruta);
            }
        } catch (Exception e) {
            Logger.getLogger(ConsultaRuta.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    public boolean insertarRuta(String origen, String destino, String distancia) {
        try {
            //ResultSet rs = st.executeQuery(query);
            String query = "insert into ruta (id,origen,destino,distancia)" + " values (null, ?, ?, ?)";
            // create the mysql insert preparedstatement
            System.out.println("or: " + origen);
            System.out.println("des: " + destino);
            System.out.println("dis: " + distancia);
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, origen);
            preparedStmt.setString(2, destino);
            preparedStmt.setString(3, distancia);
            preparedStmt.execute();
            return true;
        } catch (Exception e) {
            Logger.getLogger(ConsultaRuta.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }

    }

    public boolean modificarRuta(int id, String origen, String destino, String distancia) {
        try {
            String query = "update ruta set origen = ?, destino = ?, distancia = ? where id = ? ";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, origen);
            preparedStmt.setString(2, destino);
            preparedStmt.setString(3, distancia);
            preparedStmt.setInt(4, id);
            preparedStmt.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            return false;
        }
    }

    public boolean eliminarRuta(int id) {
        try {

            String query = "delete from ruta where id = ?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setInt(1, id);
            preparedStmt.execute();
            return true;

        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            return false;
        }

    }

}
