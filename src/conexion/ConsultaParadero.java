/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import ModeloDatos.Paradero;
import ModeloDatos.Salida;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author crist
 */
public class ConsultaParadero {

    private Connection conexion;
    private ArrayList<Paradero> paraderos = new ArrayList<Paradero>();

    public ConsultaParadero(Connection conexion) {
        this.conexion = conexion;
    }

    public void mostrarParadero() {
        try {
            String query = "SELECT * FROM paradero";
            Statement st = (Statement) conexion.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                String id = rs.getString("id");
                String nombre = rs.getString("nombre");
                String ubicacion = rs.getString("ubicacion");
                //print the results
                System.out.format("%s, %s, %s\n", id, nombre, ubicacion);
                Paradero paradero = new Paradero(id, nombre, ubicacion);
                paraderos.add(paradero);
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }

    public void consultaParaderosPorRuta(int idParadero) {
        try {
            String query = "SELECT\n"
                    + "paradero.id,\n"
                    + "paradero.nombre,\n"
                    + "paradero.ubicacion\n"
                    + "FROM\n"
                    + "ruta\n"
                    + "INNER JOIN rutatieneparadero ON rutatieneparadero.fk_ruta = ruta.id\n"
                    + "INNER JOIN paradero ON rutatieneparadero.fk_paradero = paradero.id\n"
                    + "WHERE ruta.id=?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setInt(1, idParadero);
            //Statement st = (Statement) conexion.createStatement();
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                String id = rs.getString("id");
                String nombre = rs.getString("nombre");
                String ubicacion = rs.getString("ubicacion");
                //print the results
                System.out.format("%s, %s, %s\n", id, nombre, ubicacion);
                Paradero paradero = new Paradero(id, nombre, ubicacion);
                paraderos.add(paradero);
            }
            rs.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }

    }

    public boolean insertarParadero(String nombre, String ubicacion) {
        try {
            String query = " insert into paradero (nombre, ubicacion)"
                    + " values (?, ?)";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, nombre);
            preparedStmt.setString(2, ubicacion);
            preparedStmt.execute();
            return true;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
            return false;
        }
    }
    
    public boolean eliminarParadero(int id) {
        try {

            String query = "delete from paradero where id = ?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setInt(1, id);
            preparedStmt.execute();
            return true;

        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            return false;
        }

    }
    
    public boolean modificarParadero(int id, String nombre, String ubicacion) {
        try {
            String query = "update paradero set nombre = ?, ubicacion = ? where id = ? ";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, nombre);
            preparedStmt.setString(2, ubicacion);
            preparedStmt.setInt(3, id);
            preparedStmt.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            return false;
        }
    }
    
    public int getUltimoParadero() {
        try {
            //SELECT * FROM `usuario` ORDER BY `etiqueta` DESC LIMIT 1
            int id=0;
            String query = " SELECT * FROM paradero ORDER BY id DESC LIMIT 1";
            Statement Stmt = (Statement )conexion.createStatement();
            ResultSet rs = Stmt.executeQuery(query);
            while (rs.next()) {
                id = rs.getInt("id");
            }
            return id;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
        return 0;
    }

    public ArrayList<Paradero> getParaderos() {
        return paraderos;
    }

}
