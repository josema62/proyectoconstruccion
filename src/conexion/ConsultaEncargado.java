/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author ovidio
 */
public class ConsultaEncargado
{
    private Connection conexion;
    private String RUT;
    private String PASSWORD;

    public ConsultaEncargado(Connection con) {
        this.conexion = con;
        RUT = "";
        PASSWORD = "";
    }

    public String getNombreUsuarioEncargado(String nombreUSuario2) {//rut=login
        try {
            String query = "SELECT * FROM encargado";

            // create the java statement
            Statement st = (Statement) conexion.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);
                
            while (rs.next()) {
                String rut = rs.getString("rut");
                String nombre = rs.getString("nombre");
                String apPaterno = rs.getString("apPaterno");
                String apMaterno= rs.getString("apMaterno");
                String nombreUsuario = rs.getString("nombreUsuario");
                String password = rs.getString("password");
                // print the results
                //System.out.format("%s, %s, %s, %s, %s, %s\n", rut, nombre, apPaterno, apMaterno, nombreUsuario, password);
                if(nombreUsuario.equals(nombreUSuario2))
                {
                    st.close();
                    return password;
                }
            }
            st.close();
            //return rut;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
        return "-1";
    }
    
    /*try {
            String query = "delete from encargado where rut =?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, rut);
            preparedStmt.execute();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }*/
    
    public void mostrareEncargado() {
        try {
            String query = "SELECT * FROM encargado";

            // create the java statement
            Statement st = (Statement) conexion.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next()) {
                String rut = rs.getString("rut");
                String nombre = rs.getString("nombre");
                String apPaterno = rs.getString("apPaterno");
                String apMaterno= rs.getString("apMaterno");
                String nombreUsuario = rs.getString("nombreUsuario");
                String password = rs.getString("password");
                // print the results
                System.out.format("%s, %s, %s, %s, %s, %s\n", rut, nombre, apPaterno, apMaterno, nombreUsuario, password);
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }

    public void insertarEncargado(String rut,String nombre,String apPaterno,String apMaterno,String nombreUsuario,String password) {
        try {
            String query = " insert into encargado (rut, nombre, apPaterno, apMaterno, nombreUsuario, password)"
                    + " values (?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, rut);
            preparedStmt.setString(2, nombre);
            preparedStmt.setString(3, apPaterno);
            preparedStmt.setString(4, apMaterno);
            preparedStmt.setString(5, nombreUsuario);
            preparedStmt.setString(6, password);
            preparedStmt.execute();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }

    public void modificarEncargado(String rut,String nombre,String apPaterno,String apMaterno,String nombreUsuario,String password) {
        try {
            String query = "update encargado set nombre = ?, apPaterno=?, apMaterno=?, nombreUsuario=?, password=? where rut = ? ";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, rut);
            preparedStmt.setString(2, nombre);
            preparedStmt.setString(3, apPaterno);
            preparedStmt.setString(4, apMaterno);
            preparedStmt.setString(5, nombreUsuario);
            preparedStmt.setString(6, password);
            preparedStmt.executeUpdate();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }

    public void eliminarEncargado(String rut) {
        try {
            String query = "delete from encargado where rut =?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, rut);
            preparedStmt.execute();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }
}
