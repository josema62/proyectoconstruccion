/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import ModeloDatos.Conductor;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author ovidio
 */
public class ConsultaBusTieneAlerta
{
     private Connection conexion;

    public ConsultaBusTieneAlerta(Connection con) {
        this.conexion = con;
    }

    public void mostrarChofer() {
        try {
            String query = "SELECT * FROM bustienealerta";

            // create the java statement
            Statement st = (Statement) conexion.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next()) {
                String bus = rs.getString("fk_bus");
                int alerta = rs.getInt("fk_idalerta");
                String fecha = rs.getString("fecha");
                // print the results
                System.out.format("%s, %s, %s\n", bus, alerta, fecha);
                //Conductor conductor = new Conductor(rut, nombre, apPaterno, apMaterno, direccion, telefono,licencias,experiencia);
                //conductores.add(conductor);
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }

    public void insertarBusTieneAlert(String bus, int alerta, String fecha) {
        try {
            String query = " insert into bustienealerta (fk_bus, fk_idalerta, fecha)"
                    + " values (?, ?, ?)";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, bus);
            preparedStmt.setInt(2, alerta);
            preparedStmt.setString(3, fecha);
            preparedStmt.execute();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }

    public void modificarBusTieneAlerta(String bus, int alerta, String fecha) {
        try {
            String query = "update bustienealerta set fk_idalerta=?, fecha=? where fk_bus";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setInt(2, alerta);
            preparedStmt.setString(3, fecha);
            preparedStmt.executeUpdate();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }

    public void eliminarBusTieneAlerta(String bus) {
        try {
            String query = "delete from bustienealerta where fk_bus =?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, bus);
            preparedStmt.execute();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }

}
