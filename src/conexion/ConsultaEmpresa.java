/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

/**
 *
 * @author Josema
 */

import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
public class ConsultaEmpresa {
    
    private Connection conexion;
    
     public ConsultaEmpresa(Connection con){
        
        this.conexion = con;
    }
    
    public void mostrarEmpresa(){
    try {
            String query = "SELECT * FROM empresa";

            // create the java statement
            Statement st = (Statement) conexion.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next()) {
                String rut = rs.getString("rut");
                String nombre = rs.getString("nombre");
                String ubicacion = rs.getString("ubicacion");
                String telefono = rs.getString("telefono");
                String fk_empresa = rs.getString("fk_empresa");
                String fk_empleado = rs.getString("fk_empleado");
                // print the results
                System.out.format("%s, %s, %s, %s, %s, %s, %s\n", rut, nombre, ubicacion, telefono, fk_empresa, fk_empleado);
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    
    
    }
    
    public void insertarEmpresa(String rut, String nombre, String ubicacion, String telefono, String fk_empresa, String fk_empleado){
    
        try {
            String query = " insert into empresa (rut, nombre, ubicacion, telefono, fk_empresa, fk_empleado)"
                    + " values (?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, rut);
            preparedStmt.setString(2, nombre);
            preparedStmt.setString(3, ubicacion);
            preparedStmt.setString(4, telefono);
            preparedStmt.setString(5, fk_empresa);
            preparedStmt.setString(6, fk_empleado);
            preparedStmt.execute();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
        
        
    }
    
    public void modificarEmpresa(String rut, String nombre, String ubicacion, String telefono, String fk_empresa, String fk_empleado){
    
        try {
            String query = "update empresa set rut = ?, nombre =?, ubicacion=?, telefono=?, where rut = ? ";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, rut);
            preparedStmt.setString(2, nombre);
            preparedStmt.setString(3, ubicacion);
            preparedStmt.setString(4, telefono);            
            preparedStmt.setString(5, fk_empresa);
            preparedStmt.setString(6, fk_empleado);
            preparedStmt.executeUpdate();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }
    
    public void elminarEmpresa(String rut){
    
         try {
            String query = "delete from empresa where rut =?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, rut);
            preparedStmt.execute();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }
    
}
