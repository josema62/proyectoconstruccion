/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

/**
 *
 * @author ovidio
 */
public class ConsultaAnden
{
    private Connection conexion;

    public ConsultaAnden(Connection con) {
        this.conexion = con;
    }

    public void mostrarAnden() {
        try {
            String query = "SELECT * FROM asignaanden";

            // create the java statement
            Statement st = (Statement) conexion.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next()) {
                String fk_rut = rs.getString("fk_rut");
                String fk_patente = rs.getString("fk_patente");
                int anden = rs.getInt("numeroAnden");
                //Date fecha= rs.getDate("fecha");
                String fecha= rs.getString("fecha");
                String estado= rs.getString("estado");
                // print the results
                System.out.format("%s, %s, %s, %s, %s \n", fk_rut, fk_patente, anden, fecha, estado);
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! andan1");
            System.err.println(e.getMessage());
        }
    }

    public void insertarAnden(String fk_rut, String fk_patente, int anden, String fecha, String estado) {
        try {
            String query = " insert into asignaanden (fk_rut, fk_patente, numeroAnden, fecha, estado)"
                    + " values (?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, fk_rut);
            preparedStmt.setString(2, fk_patente);
            preparedStmt.setInt(3, anden);
            //preparedStmt.setDate(4, (java.sql.Date) fecha);
            preparedStmt.setString(4, fecha);
            preparedStmt.setString(5, estado);
            preparedStmt.execute();
            //conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!pruebaaaaaaa");
            System.err.println(e.getMessage());
        }
    }

    public void modificarAnden(String rut, String patente, String fecha ,int anden) {
        try {
            String query = "update asignaanden set fk_rut=?, fk_patente=?, fecha = ? where numeroAnden = ? ";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, rut);
            preparedStmt.setString(2, patente);
            preparedStmt.setInt(3, anden);
            //preparedStmt.setDate(4, (java.sql.Date) fecha);
            preparedStmt.setString(4, fecha);
            preparedStmt.executeUpdate();
            //conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! anden2");
            System.err.println(e.getMessage());
        }
    }

    public void eliminarAnden(String rut, String patente, String fecha,int anden) {
        try {
            String query = "delete from asignaanden where fk_rut=? and fk_patente=? and numeroAnden =? and fecha=?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setInt(1, anden);
            preparedStmt.execute();
            //conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! anden3");
            System.err.println(e.getMessage());
        }
    }

}
