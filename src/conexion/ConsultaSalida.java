/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import ModeloDatos.Conductor;
import ModeloDatos.Salida;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author crist
 */
public class ConsultaSalida {

    private Connection conexion;
    private ArrayList<Salida> salidas = new ArrayList<Salida>();

    public ConsultaSalida(Connection con) {
        this.conexion = con;
    }

    public void mostrarSalida() {
        try {
            String query = "SELECT * FROM salida";
            Statement st = (Statement) conexion.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                Date fecha = rs.getDate("fecha");
                Time horaSalida = rs.getTime("horaSalida");
                Time horaLlegada = rs.getTime("horaLlegada");
                Time tiempoEstimado = rs.getTime("tiempoEstimado");
                // print the results
                //System.out.format("%s, %s, %s, %s, %s, %s, %s, %s, %s\n", rut, nombre, apPaterno, apMaterno, direccion, telefono, licencias, experiencia, fk_empresa);
                Salida salida = new Salida(horaSalida, horaLlegada, fecha, tiempoEstimado);
                salidas.add(salida);
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }

    public ArrayList<Salida> getSalidas() {
        return salidas;
    }

}
