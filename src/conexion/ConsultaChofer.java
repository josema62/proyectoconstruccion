/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import ModeloDatos.Conductor;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author ovidio
 */
public class ConsultaChofer {

    private Connection conexion;
    private ArrayList<Conductor> conductores = new ArrayList<Conductor>();

    public ConsultaChofer(Connection con) {
        this.conexion = con;
    }

    public void mostrarChofer(String fk_empresa) {
        try {
            String query = "SELECT\n"
                    + "empleadoconductor.rut,\n"
                    + "empleadoconductor.nombre,\n"
                    + "empleadoconductor.apPaterno,\n"
                    + "empleadoconductor.apMaterno,\n"
                    + "empleadoconductor.direccion,\n"
                    + "empleadoconductor.telefono,\n"
                    + "empleadoconductor.cargo,\n"
                    + "empleadoconductor.mail,\n"
                    + "empleadoconductor.fk_bus\n"
                    + "FROM\n"
                    + "empresa\n"
                    + "INNER JOIN empresatieneempleado ON empresatieneempleado.fk_empresa = empresa.rut\n"
                    + "INNER JOIN empleadoconductor ON empresatieneempleado.fk_empleado = empleadoconductor.rut\n"
                    + "WHERE empresa.rut=?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, fk_empresa);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                String rut = rs.getString("rut");
                String nombre = rs.getString("nombre");
                String apPaterno = rs.getString("apPaterno");
                String apMaterno = rs.getString("apMaterno");
                String direccion = rs.getString("direccion");
                String telefono = rs.getString("telefono");
                String licencias = rs.getString("cargo");
                String mail = rs.getString("mail");
                
                System.out.format("%s, %s, %s, %s, %s, %s, %s\n", rut, nombre, apPaterno, apMaterno, direccion, telefono, licencias, mail, fk_empresa);
                Conductor conductor = new Conductor(rut, nombre, apPaterno, apMaterno, direccion, telefono, licencias, mail);
                conductores.add(conductor);
            }
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }

    public String getChofer(String rut2) {//rut=login

        try {
            String query = "SELECT * FROM empleadoconductor";

            // create the java statement
            Statement st = (Statement) conexion.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next()) {
                String rut = rs.getString("rut");
                String nombre = rs.getString("nombre");
                String apPaterno = rs.getString("apPaterno");
                String apMaterno = rs.getString("apMaterno");
                String direccion = rs.getString("direccion");
                String telefono = rs.getString("telefono");
                String cargo = rs.getString("cargo");
                String mail = rs.getString("mail");
                // print the results
                System.out.format("%s, %s, %s, %s, %s, %s, %s, %s\n", rut, nombre, apPaterno, apMaterno, direccion, telefono, cargo, mail);
                if (rut.equals(rut2)) {
                    st.close();
                    return rut;
                }
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }

        return "-1";
    }

    public boolean insertarChofer(String rut, String nombre, String apPaterno, String apMaterno, String direccion, String telefono, String cargo, String mail) {
        try {
            String query = " insert into empleadoconductor (rut, nombre, apPaterno, apMaterno, direccion, telefono, cargo, mail)"
                    + " values (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, rut);
            preparedStmt.setString(2, nombre);
            preparedStmt.setString(3, apPaterno);
            preparedStmt.setString(4, apMaterno);
            preparedStmt.setString(5, direccion);
            preparedStmt.setString(6, telefono);
            preparedStmt.setString(7, cargo);
            preparedStmt.setString(8, mail);
            
            preparedStmt.execute();
            return true;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
            return false;
        }
    }
    public boolean insertarEmpresaTieneEmpleado(String fk_empleado,String fk_empresa){
        try {
            System.out.println("empresa: "+fk_empresa);
            System.out.println("empleado: "+fk_empleado);
            String query = " insert into empresatieneempleado (fk_empleado, fk_empresa)"
                    + " values (?, ?)";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, fk_empleado);
            preparedStmt.setString(2, fk_empresa);    
            preparedStmt.execute();
            return true;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
            return false;
        }
        
    }

    public boolean modificarChofer(String rut, String nombre, String apPaterno, String apMaterno, String direccion, String telefono, String cargo, String mail) {
        try {
            String query = "update empleadoconductor set nombre = ?, apPaterno=?, apMaterno=?, direccion=?, telefono=?, cargo=?, mail=? where rut = ? ";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, nombre);
            preparedStmt.setString(2, apPaterno);
            preparedStmt.setString(3, apMaterno);
            preparedStmt.setString(4, direccion);
            preparedStmt.setString(5, telefono);
            preparedStmt.setString(6, cargo);
            preparedStmt.setString(7, mail);
            preparedStmt.setString(8, rut);
            preparedStmt.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
            return false;
        }
    }

    public void eliminarChofer(String rut) {
        try {
            String query = "delete from empleadoconductor where rut =?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, rut);
            preparedStmt.execute();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }
    public boolean eliminarEmpresaTieneEmpleado(String fk_empleado){
        try {
            String query = "delete from empresatieneempleado where fk_empleado =?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, fk_empleado);
            preparedStmt.execute();
            return true;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
            return false;
        }
    }

    public ArrayList<Conductor> getConductores() {
        return conductores;
    }

    public String getCorreo(String rut2) {//rut=login

        try {
            String query = "SELECT * FROM empleadoconductor";

            // create the java statement
            Statement st = (Statement) conexion.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next()) {
                String rut = rs.getString("rut");
                String nombre = rs.getString("nombre");
                String apPaterno = rs.getString("apPaterno");
                String apMaterno = rs.getString("apMaterno");
                String direccion = rs.getString("direccion");
                String telefono = rs.getString("telefono");
                String cargo = rs.getString("cargo");
                String mail = rs.getString("mail");
                // print the results
                System.out.format("%s, %s, %s, %s, %s, %s, %s, %s\n", rut, nombre, apPaterno, apMaterno, direccion, telefono, cargo, mail);
                if (rut.equals(rut2)) {
                    st.close();
                    return mail;
                }
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }

        return "-1";
    }

}
