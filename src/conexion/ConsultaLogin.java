/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.ResultSet;

/**
 *
 * @author crist
 */
public class ConsultaLogin {

    private Connection conexion;
    String nombreUsuario;
    String password;
    String fk_empresa;

    public ConsultaLogin(Connection con) {
        this.conexion = con;
    }

    public boolean consultaEncargado(String usuario, String contraseña) {
        try {
            String query = "SELECT * FROM encargado";

            // create the java statement
            Statement st = (Statement) conexion.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next()) {
                nombreUsuario = rs.getString("nombreUsuario");
                password = rs.getString("password");
                System.out.println("usuario: " + nombreUsuario);
                System.out.println("contraseña: " + password);
                if (nombreUsuario.equalsIgnoreCase(usuario) && password.equalsIgnoreCase(contraseña)) {
                    return true;
                }

            }
            st.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
        return false;
    }

    public boolean consultaAdministrador(String usuario, String contraseña) {
        try {
            String query2 = "SELECT * FROM empleadoadministrador";
            // create the java statement
            Statement st2 = (Statement) conexion.createStatement();
            // execute the query, and get a java resultset
            ResultSet rs2 = st2.executeQuery(query2);
            // iterate through the java resultset
            while (rs2.next()) {
                nombreUsuario = rs2.getString("nombreUsuario");
                password = rs2.getString("password");
                fk_empresa = rs2.getString("fk_empresa");
                System.out.println("usuario: " + nombreUsuario);
                System.out.println("contraseña: " + password);
                if (nombreUsuario.equalsIgnoreCase(usuario) && password.equalsIgnoreCase(contraseña)) {
                    return true;
                }
            }
            st2.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
        return false;
    }

    public String getFk_empresa() {
        return fk_empresa;
    }
    
}
