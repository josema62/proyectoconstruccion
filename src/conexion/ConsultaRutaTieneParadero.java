/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author crist
 */
public class ConsultaRutaTieneParadero {

    private Connection conexion;

    public ConsultaRutaTieneParadero(Connection conexion) {
        this.conexion = conexion;
    }

    public boolean insertarRutaTieneParadero(int fk_ruta, int fk_paradero) {
        try {
            String query = " insert into rutatieneparadero (fk_ruta, fk_paradero)"
                    + " values (?, ?)";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setInt(1, fk_ruta);
            preparedStmt.setInt(2, fk_paradero);
            preparedStmt.execute();
            return true;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
            return false;
        }

    }

}
