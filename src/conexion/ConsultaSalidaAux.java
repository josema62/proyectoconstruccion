/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import ModeloDatos.Paradero;
import ModeloDatos.SalidaAux;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;

/**
 *
 * @author Usuario
 */
public class ConsultaSalidaAux {

    private Connection conexion;
    private ArrayList<SalidaAux> salidas;

    public ConsultaSalidaAux(Connection con) {
        this.conexion = con;
        this.salidas = new ArrayList<>();
    }

    public boolean actualizarSalida(String idSalida, String nuevaFechaSalida, String nuevaFechaEntrada) {
        String query = "UPDATE salidaaux\n"
                + "	SET fechaSalida = ?, fechaEntrada = ?\n"
                + "	WHERE id = ?;";
        try {
            PreparedStatement preparedStmt = conexion.prepareStatement(query);

            preparedStmt.setString(1, nuevaFechaSalida);
            preparedStmt.setString(2, nuevaFechaEntrada);
            preparedStmt.setString(3, idSalida);
            preparedStmt.execute();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(ConsultaHora.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;

    }

    public void salidas(String patenteBus) {
        String query = "SELECT salidaaux.id,salidaaux.fechaSalida, salidaaux.fechaEntrada, salidaaux.bus\n"
                + "	FROM bus inner join salidaaux on bus.patente = salidaaux.bus\n"
                + "	WHERE bus.patente = ?;";

        PreparedStatement preparedStmt;
        try {
            preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, patenteBus);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String fechaSalida = rs.getString("fechaSalida");
                String fechaEntrada = rs.getString("fechaEntrada");
                String patente = rs.getString("bus");
                SalidaAux salidaAux = new SalidaAux(String.valueOf(id), fechaSalida, fechaEntrada, patente);
                salidas.add(salidaAux);
                /*
                comboLista.add(fechaSalida+"-"+fechaEntrada);
                salidaBus.setId(String.valueOf(id));
                salidaBus.setFechaSalida(fechaSalida);
                salidaBus.setFechaEntrada(fechaEntrada);
                 */

            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaSalidaAux.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void mostrarSalida(String fk_empresa) {
        System.out.println("entre al mostrar salida");
        try {
            String query = "SELECT\n"
                    + "salidaaux.id,\n"
                    + "salidaaux.fechaSalida,\n"
                    + "salidaaux.fechaEntrada,\n"
                    + "salidaaux.bus\n"
                    + "FROM\n"
                    + "salidaaux\n"
                    + "INNER JOIN bus ON salidaaux.bus = bus.patente\n"
                    + "INNER JOIN empresatienebus ON empresatienebus.fk_bus = bus.patente\n"
                    + "WHERE fk_empresa=?";

            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, fk_empresa);
            ResultSet rs = preparedStmt.executeQuery();
            System.out.println("antes de entrar al while");
            while (rs.next()) {
                System.out.println("entre al while");

                String id = rs.getString("id");
                String salida = rs.getString("fechaSalida");
                String entrada = rs.getString("fechaEntrada");
                String patente = rs.getString("bus");

                System.out.format("%s, %s, %s, %s\n", id, salida, entrada, patente);

                SalidaAux salidaAux = new SalidaAux(id, salida, entrada, patente);
                salidas.add(salidaAux);

            }
            System.out.println("sali del while");
        } catch (Exception e) {
            Logger.getLogger(ConsultaRuta.class.getName()).log(Level.SEVERE, null, e);
        }

    }
    
    public boolean agregarSalida(String fechaSalida, String fechaEntrada, String bus) {
        try {
            String query = "insert into salidaaux (id,fechaSalida,fechaEntrada,bus)" + " values (null, ?, ?, ?)";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, fechaSalida);
            preparedStmt.setString(2, fechaEntrada);
            preparedStmt.setString(3, bus);
            preparedStmt.execute();
            return true;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! bus1");
            System.err.println(e.getMessage());
            return false;
        }

    }
    
    public boolean modificarSalida(String id, String fechaSalida, String fechaEntrada, String bus) {
        try {
            String query = "update salidaaux set fechaSalida = ?, fechaEntrada = ?, bus = ? where id = ? ";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, fechaSalida);
            preparedStmt.setString(2, fechaEntrada);
            preparedStmt.setString(3, bus);
            preparedStmt.setString(4, id);
            preparedStmt.execute();
            return true;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! bus1");
            System.err.println(e.getMessage());
            return false;
        }

    }
    public boolean eliminarSalidaAux(String id) {
        try {

            String query = "delete from salidaaux where id = ?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, id);
            preparedStmt.execute();
            return true;

        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            return false;
        }

    }

    public ArrayList<SalidaAux> getSalidas() {
        return salidas;
    }

}
