/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import ModeloDatos.Bus;
import ModeloDatos.Paradero;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author crist
 */
public class ConsultaBus {

    //mostrar buses
    private Connection conexion;
    private ArrayList<Bus> buses = new ArrayList<Bus>();

    public ConsultaBus(Connection con) {
        this.conexion = con;
    }

    public String getPatente(String patente2) {//rut=login
        
        try {
            String query = "SELECT * FROM bus";

            // create the java statement
            Statement st = (Statement) conexion.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next()) {
                String patente = rs.getString("patente");
                String marca = rs.getString("marca");
                String modelo = rs.getString("modelo");
                int anio = rs.getInt("año");
                int capacidad = rs.getInt("capacidad");
                String fk_conductor = rs.getString("fk_conductor");
                // print the results
                System.out.format("%s, %s, %s, %s, %s, %s\n", patente, marca, modelo, anio, capacidad, fk_conductor);
                
                if(patente.equals(patente2))
                {
                    st.close();
                    return patente;
                }
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! bus1");
            System.err.println(e.getMessage());
        }
       
        return "-1";
    }

    public ArrayList<Bus> datosBus() {
        try {
            String query = "SELECT * FROM bus";

            // create the java statement
            Statement st = (Statement) conexion.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next()) {
                String patente = rs.getString("patente");
                String marca = rs.getString("marca");
                String modelo = rs.getString("modelo");
                int anio = rs.getInt("año");
                int capacidad = rs.getInt("capacidad");
                String fk_conductor = rs.getString("fk_conductor");
                // print the results
                System.out.format("%s, %s, %s, %s, %s, %s\n", patente, marca, modelo, anio, capacidad, fk_conductor);
                Bus bus = new Bus(patente, marca, modelo, anio, capacidad, fk_conductor);
                buses.add(bus);
            }
            st.close();
            return buses;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! bus2");
            System.err.println(e.getMessage());
        }
        return null;
    }

    public void mostrarBus(String fk_empresa) {
        try {
            String query = "SELECT\n"
                    + "bus.patente,\n"
                    + "bus.marca,\n"
                    + "bus.modelo,\n"
                    + "bus.`año`,\n"
                    + "bus.capacidad,\n"
                    + "bus.fk_conductor\n"
                    + "FROM\n"
                    + "empresa\n"
                    + "INNER JOIN empresatienebus ON empresatienebus.fk_empresa = empresa.rut\n"
                    + "INNER JOIN bus ON empresatienebus.fk_bus = bus.patente\n"
                    + "WHERE empresa.rut=?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, fk_empresa);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                String patente = rs.getString("patente");
                String marca = rs.getString("marca");
                String modelo = rs.getString("modelo");
                int anio = rs.getInt("año");
                int capacidad = rs.getInt("capacidad");
                String fk_conductor = rs.getString("fk_conductor");
                // print the results
                System.out.format("%s, %s, %s, %s, %s, %s\n", patente, marca, modelo, anio, capacidad, fk_conductor);
                Bus bus = new Bus(patente, marca, modelo, anio, capacidad, fk_conductor);
                buses.add(bus);
            }
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! bus3");
            System.err.println(e.getMessage());
        }
    }

    public void mostrarTodosLosBuses() {
        try {
            String query = "select * from bus";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                String patente = rs.getString("patente");
                String marca = rs.getString("marca");
                String modelo = rs.getString("modelo");
                int anio = rs.getInt("año");
                int capacidad = rs.getInt("capacidad");
                String fk_conductor = rs.getString("fk_conductor");
                // print the results
                System.out.format("%s, %s, %s, %s, %s, %s\n", patente, marca, modelo, anio, capacidad, fk_conductor);
                Bus bus = new Bus(patente, marca, modelo, anio, capacidad, fk_conductor);
                buses.add(bus);
            }
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! bus3");
            System.err.println(e.getMessage());
        }
    }

    public boolean insertarBus(String patente, String marca, String modelo, int anio, int capacidad, String fk_conductor) {
        try {
            String query = " insert into bus (patente, marca, modelo, año, capacidad, fk_conductor)"
                    + " values (?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, patente);
            preparedStmt.setString(2, marca);
            preparedStmt.setString(3, modelo);
            preparedStmt.setInt(4, anio);
            preparedStmt.setInt(5, capacidad);
            preparedStmt.setString(6, fk_conductor);
            preparedStmt.execute();
            return true;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! bus4");
            System.err.println(e.getMessage());
            return false;
        }
    }

    public boolean insertarEmpresaTieneBus(String patente, String fk_empresa) {
        try {
            String query = " insert into empresatienebus (fk_bus, fk_empresa)"
                    + " values (?, ?)";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, patente);
            preparedStmt.setString(2, fk_empresa);
            preparedStmt.execute();
            return true;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! bus4");
            System.err.println(e.getMessage());
            return false;
        }
    }

    public boolean modificarBus(String patente, String marca, String modelo, int año, int capacidad, String fk_conductor) {
        try {
            String query = "update bus set marca=?,modelo=?,año=?,capacidad=?, fk_conductor=? where patente = ? ";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(6, patente);
            preparedStmt.setString(1, marca);
            preparedStmt.setString(2, modelo);
            preparedStmt.setInt(3, año);
            preparedStmt.setInt(4, capacidad);
            preparedStmt.setString(5, fk_conductor);
            preparedStmt.executeUpdate();
            return true;
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! bus5");
            System.err.println(e.getMessage());
            return false;
        }
    }
    public void modificarBus2(String patente, int multa) {
        try {
            String query = "update bus set multa = ? where patente = ? ";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setInt(1, multa);
            preparedStmt.setString(2, patente);
            preparedStmt.executeUpdate();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! bus5");
            System.err.println(e.getMessage());
        }
    }
    
    

    public void eliminarBus(String patente) {
        try {
            String query = "delete from bus where patente =?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, patente);
            preparedStmt.execute();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! bus6");
            System.err.println(e.getMessage());
        }
    }

    public void eliminarEmpresaTieneBus(String patente) {
        try {
            String query = "delete from empresatienebus where fk_bus =?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, patente);
            preparedStmt.execute();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error! bus3232");
            System.err.println(e.getMessage());
        }
    }

    public String rutConductorBus(String patenteBus) {

        try {
            String query = "select empleadoconductor.rut\n"
                    + "from empleadoconductor inner join bus on empleadoconductor.rut = bus.fk_conductor\n"
                    + "where bus.patente = ?;";

            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, patenteBus);

            ResultSet rs = preparedStmt.executeQuery();
            String rutConductor = "";
            while (rs.next()) {
                rutConductor = rs.getString("rut");
            }
            return rutConductor;
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaHora.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "vacio";
    }

    public ArrayList<Bus> getBuses() {
        return buses;
    }

}
