/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author ovidio
 */
public class ConsultaEmpleadoAdministrador
{
    private Connection conexion;

    public ConsultaEmpleadoAdministrador(Connection con) {
        this.conexion = con;
    }

    public void mostrarAmdinistrador() {
        try {
            String query = "SELECT * FROM empleadoadministrador";

            // create the java statement
            Statement st = (Statement) conexion.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next()) {
                String rut = rs.getString("rut");
                String nombre = rs.getString("nombre");
                String apPaterno = rs.getString("apPaterno");
                String apMaterno= rs.getString("apMaterno");
                String direccion= rs.getString("direccion");
                String telefono = rs.getString("telefono");
                String nombreUsuario = rs.getString("nombreUsuario");
                String password = rs.getString("password");
                String fk_empresa = rs.getString("fk_empresa");
                // print the results
                System.out.format("%s, %s, %s, %s, %s, %s, %s,%s, %s\n", rut, nombre, apPaterno, apMaterno, direccion, telefono, nombreUsuario, password, fk_empresa);
            }
            st.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }

    public void insertarAdministrador(String rut,String nombre,String apPaterno,String apMaterno,String direccion,String telefono,String nombreUsuario,String password,String fk_empresa) {
        try {
            String query = " insert into empleadoadministrador (rut, nombre, apPaterno, apMaterno, direccion, telefono, nombreUsuario, password, fk_empresa)"
                    + " values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, rut);
            preparedStmt.setString(2, nombre);
            preparedStmt.setString(3, apPaterno);
            preparedStmt.setString(4, apMaterno);
            preparedStmt.setString(5, direccion);
            preparedStmt.setString(6, telefono);
            preparedStmt.setString(7, nombreUsuario);
            preparedStmt.setString(8, password);
            preparedStmt.setString(9, fk_empresa);
            preparedStmt.execute();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }

    public void modificarAdministrador(String rut,String nombre,String apPaterno,String apMaterno,String direccion,String telefono,String nombreUsuario,String password,String fk_empresa) {
        try {
            String query = "update empleadoadministrador set nombre = ?, apPaterno=?, apMaterno=?, direccion=?, telefono=?, nombreUsuario=?, password=? where rut = ? ";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, nombre);
            preparedStmt.setString(2, apPaterno);
            preparedStmt.setString(3, apMaterno);
            preparedStmt.setString(4, direccion);
            preparedStmt.setString(5, telefono);
            preparedStmt.setString(6, nombreUsuario);
            preparedStmt.setString(7, password);
            preparedStmt.setString(8, fk_empresa);
            preparedStmt.executeUpdate();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }

    public void eliminarAdministrador(String rut) {
        try {
            String query = "delete from empleadoadministrador where rut =?";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, rut);
            preparedStmt.execute();
            conexion.close();
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }
}
