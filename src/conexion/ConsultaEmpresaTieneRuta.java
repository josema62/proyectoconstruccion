/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import ModeloDatos.Ruta;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author crist
 */
public class ConsultaEmpresaTieneRuta {

    private Connection conexion;
    public int ultimo = 0;

    public ConsultaEmpresaTieneRuta(Connection conexion) {
        this.conexion = conexion;
    }

    public boolean insertarEmpresaTieneRuta(String fk_empresa, int fk_ruta) {
        try {
            //ResultSet rs = st.executeQuery(query);
            String query = "insert into empresatieneruta (fk_empresa,fk_ruta)" + " values (?, ?)";
            // create the mysql insert preparedstatement
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, fk_empresa);
            preparedStmt.setInt(2, fk_ruta);
            System.out.println("rutaaaa: " + fk_ruta);
            preparedStmt.execute();
            return true;
        } catch (Exception e) {
            Logger.getLogger(ConsultaRuta.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    public int ultimoRutaAgregada() {
        try {
            //ResultSet rs = st.executeQuery(query);
            String query = "select MAX(id) as id from ruta";
            // create the mysql insert preparedstatement
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            ResultSet rs = preparedStmt.executeQuery();
             while (rs.next()) {
                ultimo = rs.getInt("id");
                
             }
             return ultimo;
            
        } catch (Exception e) {
            Logger.getLogger(ConsultaRuta.class.getName()).log(Level.SEVERE, null, e);
            return 0;
        }
        
    }

}
