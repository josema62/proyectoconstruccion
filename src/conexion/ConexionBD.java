/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.mysql.jdbc.Statement;
import java.sql.SQLException;

/**
 *
 * @author crist
 */
public class ConexionBD {

    Connection con;

    public ConexionBD() {
        String url = "jdbc:mysql://localhost/administraciondeterminal";
        String user = "root";
        String pass = "";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, pass);
            System.out.println("base de datos conectada");
        } catch (Exception ex) {
            System.out.println("Error al conectar a la base de datos");
        }
    }

    public Connection obtenerConexion() {
        return con;
    }

}
