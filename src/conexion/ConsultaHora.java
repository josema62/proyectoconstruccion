/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import ModeloDatos.Paradero;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Usuario
 */
public class ConsultaHora {
    
    private Connection conexion;
    private ArrayList<Paradero> parederos = new ArrayList<>();
    private ArrayList<String> horarios = new ArrayList<>();
    

    public ConsultaHora(Connection conexion) {
        this.conexion = conexion;
        
    }
    
    public boolean updateHoraParadero(int id, String hora){
        String query = "update horario\n" +
                        "set hora = ?\n" +
                        "where id = ?;";
        try {
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            
            
            preparedStmt.setString(1, hora);
            preparedStmt.setInt(2, id);
            preparedStmt.execute();
            return true;
            
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaHora.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
    
    /*
    public void horariosBus(String patenteBus, String fechaSalida, String fechaEntrada){

        try {
            String query = "SELECT paradero.id,paradero.nombre, horario.hora\n" +
"	FROM bus INNER JOIN salidaaux ON bus.patente = salidaaux.bus\n" +
"					 INNER JOIN horario ON salidaaux.id = horario.salida\n" +
"					 INNER JOIN paradero ON horario.paradero = paradero.id\n" +
"	WHERE bus.patente=? AND salidaaux.fechaSalida=? AND salidaaux.fechaEntrada=?;";
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, patenteBus);
            preparedStmt.setString(2, fechaSalida);
            preparedStmt.setString(3, fechaEntrada);
            ResultSet rs = preparedStmt.executeQuery();
            while(rs.next()){
                int id = rs.getInt("id");
                String nombreParadero = rs.getString("nombre");
                String hora = rs.getString("hora");
                Paradero paradero = new Paradero(String.valueOf(id),nombreParadero,"");
                this.parederos.add(paradero);
                this.horarios.add(hora);
            
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaHora.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    */
    
    public void horariosBus(String patenteBus, String cod,String fechaSalida, String fechaEntrada){

        try {
            /*
            String query = "SELECT paradero.id,paradero.nombre, horario.hora\n" +
"	FROM bus INNER JOIN salidaaux ON bus.patente = salidaaux.bus\n" +
"					 INNER JOIN horario ON salidaaux.id = horario.salida\n" +
"					 INNER JOIN paradero ON horario.paradero = paradero.id\n" +
"	WHERE bus.patente=? AND salidaaux.fechaSalida=? AND salidaaux.fechaEntrada=?;";
            */
            String query = "SELECT paradero.id,paradero.nombre, horario.hora\n" +
"	FROM bus INNER JOIN salidaaux ON bus.patente = salidaaux.bus\n" +
"					 INNER JOIN horario ON salidaaux.id = horario.salida\n" +
"					 INNER JOIN paradero ON horario.paradero = paradero.id\n" +
"	WHERE bus.patente=? AND salidaaux.id = ?";
            
            PreparedStatement preparedStmt = conexion.prepareStatement(query);
            preparedStmt.setString(1, patenteBus);
            preparedStmt.setString(2, cod);
            //preparedStmt.setString(2, fechaSalida);
            //preparedStmt.setString(3, fechaEntrada);
            ResultSet rs = preparedStmt.executeQuery();
            while(rs.next()){
                int id = rs.getInt("id");
                String nombreParadero = rs.getString("nombre");
                String hora = rs.getString("hora");
                Paradero paradero = new Paradero(String.valueOf(id),nombreParadero,"");
                this.parederos.add(paradero);
                this.horarios.add(hora);
            
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaHora.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public ArrayList<Paradero> getParederos() {
        return this.parederos;
    }

    public ArrayList<String> getHorarios() {
        return this.horarios;
    }
    
    
    
}
