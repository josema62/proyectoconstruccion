/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloDatos;

import java.sql.Time;
import java.util.Date;

/**
 *
 * @author Josema
 */
public class Salida implements SujetoObservado {
    
    private Time horaSalida;
    private Time horaLlegada;
    private Date fecha;
    private Time tiempoEstimadoViaje;

    public Salida(Time horaSalida, Time horaLlegada, Date fecha, Time tiempoEstimadoViaje) {
        this.horaSalida = horaSalida;
        this.horaLlegada = horaLlegada;
        this.fecha = fecha;
        this.tiempoEstimadoViaje = tiempoEstimadoViaje;
    }

    public Time getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(Time horaSalida) {
        this.horaSalida = horaSalida;
    }

    public Time getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(Time horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Time getTiempoEstimadoViaje() {
        return tiempoEstimadoViaje;
    }

    public void setTiempoEstimadoViaje(Time tiempoEstimadoViaje) {
        this.tiempoEstimadoViaje = tiempoEstimadoViaje;
    }

    @Override
    public void addObservador() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteObservador() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void ModificarObservador() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
