/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloDatos;

import java.util.ArrayList;

/**
 *
 * @author Josema
 */
public class Ruta implements Observador{
    
    private int id;
    private String origen;
    private String destino;
    private String distancia;
    private ArrayList<Paradero> paraderos;
    private ArrayList<Salida> horarios;

    public Ruta(int id, String origen, String destino, String distancia, ArrayList<Paradero> paraderos, ArrayList<Salida> horarios) {
        this.id = id;
        this.origen = origen;
        this.destino = destino;
        this.distancia = distancia;
        this.paraderos = paraderos;
        this.horarios = horarios;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public ArrayList<Paradero> getParaderos() {
        return paraderos;
    }

    public void setParaderos(ArrayList<Paradero> paraderos) {
        this.paraderos = paraderos;
    }

    public ArrayList<Salida> getHorarios() {
        return horarios;
    }

    public void setHorarios(ArrayList<Salida> horarios) {
        this.horarios = horarios;
    }

    @Override
    public void actualizar() {
        
    }
    
    
    
}
