/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloDatos;

/**
 *
 * @author Josema
 */
public class Anden {
    
    private String rut;
    private String patente;
    private int numero;
    private String fecha;
    private boolean ocupado;
    private boolean reservado;
    
    public Anden()
    {
        rut = "";
        patente = "";
        ocupado = false;
        fecha = "";
        reservado = false;
    }

    public String getRut() {
        return rut;
    }
    
    public void setRut(String rut) {
        this.rut=rut;
    }
    
    public String getPatente() {
        return patente;
    }
    
    public void setPatente(String patente) {
        this.patente=patente;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    
    public String getFecha() {
        return fecha;
    }
    
    public void setFecha(String fecha) {
        this.fecha=fecha;
    }
    
    public boolean getReservado() {
        return reservado;
    }

    public void setReservado(boolean reservado) {
        this.reservado = reservado;
    }
    
    public boolean getOcupado() {
        return ocupado;
    }

    public void setOcupado(boolean ocupado) {
        this.ocupado = ocupado;
    }
    
    public void imprimirAnden()
    {
        System.out.println("Rut Chofer: "+rut);
        System.out.println("Patente Bus: "+patente);
        System.out.println("Numero Anden: "+numero);
        System.out.println("fecha: "+fecha);
        System.out.println("Reservado: "+reservado);
        System.out.println("Ocupado: "+ocupado);
    }
    
    
}
