/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloDatos;

import java.util.Date;

/**
 *
 * @author Josema
 */
public class AsignaAnden {
    
    private int numeroAnden;
    private String patente;
    private Date fecha;

    public AsignaAnden(int numeroAnden, String patente, Date fecha) {
        this.numeroAnden = numeroAnden;
        this.patente = patente;
        this.fecha = fecha;
    }

    public int getNumeroAnden() {
        return numeroAnden;
    }

    public void setNumeroAnden(int numeroAnden) {
        this.numeroAnden = numeroAnden;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    
    
}
