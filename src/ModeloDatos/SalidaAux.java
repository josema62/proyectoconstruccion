/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloDatos;

/**
 *
 * @author Usuario
 */
public class SalidaAux {
    
    private String id;
    private String fechaSalida;
    private String fechaEntrada;
    private String patente;
    
    public SalidaAux(String id, String fechaSalida, String fechaEntrada, String patente){
        this.id = id;
        this.fechaSalida = fechaSalida;
        this.fechaEntrada = fechaEntrada;
        this.patente=patente;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(String fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }
    
    
    
}
