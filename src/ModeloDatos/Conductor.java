/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloDatos;

import java.util.ArrayList;

/**
 *
 * @author Josema
 */
public class Conductor extends Empleado {

    private String licencias;
    private String experiencia;

    public Conductor(String rutEmpleado, String nombre, String apellidoPaterno, String apellidoMaterno, String direccion, String telefono, String licencias, String mail) {
        super(rutEmpleado, nombre, apellidoPaterno, apellidoMaterno, direccion, telefono);
        this.licencias = licencias;
        this.experiencia = mail;
    }

    public String getLicencias() {
        return licencias;
    }

    public void setLicencias(String licencias) {
        this.licencias = licencias;
    }

    public String getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(String experiencia) {
        this.experiencia = experiencia;
    }

}
