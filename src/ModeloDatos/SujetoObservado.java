/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloDatos;

import java.util.ArrayList;

/**
 *
 * @author Ignacio Correa
 */
public interface SujetoObservado {
    
     ArrayList<Observador> observadores = new ArrayList<Observador>();
    
    public void addObservador();
    
    public void deleteObservador();
    
    public void ModificarObservador();
}
