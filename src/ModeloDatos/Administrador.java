/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloDatos;

/**
 *
 * @author Josema
 */
public class Administrador extends Empleado {
    
    private String nombreUsuario;
    private String password;
    
    public Administrador(String rutEmpleado, String nombre, String apellidoPaterno, String apellidoMaterno, String direccion, String telefono) {
        super(rutEmpleado, nombre, apellidoPaterno, apellidoMaterno, direccion, telefono);
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    
}
