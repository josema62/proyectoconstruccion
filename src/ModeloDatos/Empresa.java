/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloDatos;

import java.util.ArrayList;

/**
 *
 * @author Josema
 */
public class Empresa {
    
    private String rutEmpresa;
    private String nombre;
    private String ubicacion;
    private ArrayList<Empleado> empleados;

    public Empresa(String rutEmpresa, String nombre, String ubicacion, ArrayList<Empleado> empleados) {
        this.rutEmpresa = rutEmpresa;
        this.nombre = nombre;
        this.ubicacion = ubicacion;
        this.empleados = empleados;
    }

    public String getRutEmpresa() {
        return rutEmpresa;
    }

    public void setRutEmpresa(String rutEmpresa) {
        this.rutEmpresa = rutEmpresa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public ArrayList<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(ArrayList<Empleado> empleados) {
        this.empleados = empleados;
    }
    
    
    
}
