/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Controlador.Alerta;
import Controlador.ControladorCorreo;
import Controlador.Correo;
import ModeloDatos.Anden;
import ModeloDatos.Bus;
import conexion.ConexionBD;
import conexion.ConsultaAlerta;
import conexion.ConsultaAnden;
import conexion.ConsultaBus;
import conexion.ConsultaChofer;
import conexion.ConsultaEncargado;
import java.awt.geom.Area;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
//import java.util.Timer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import conexion.ConsultaBusTieneAlerta;
import java.awt.Dimension;
import java.awt.Toolkit;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.SceneAntialiasing;
import javafx.util.Duration;

/**
 *
 * @author Usuario
 */
public class VistaEncargado extends BorderPane{
    
    
    private double w;
    private double h;
    
   
    
    private Button anden3, anden4, anden5,anden6,anden7,anden8,anden9,anden10,anden11,anden12,anden13,anden14,anden15,anden16,anden17,anden18,anden19,anden20,
            anden21,anden22,anden23,anden24,anden25,anden26,anden27,anden28,anden29,anden30,anden31,anden32,anden33,anden34,anden35,anden36;
    
    private conexion.ConexionBD conexion;
    private ConsultaAnden consultaAnden;
    
    private ArrayList<Anden> anden;
    
    private Button aceptar, cancelar, reservado, ocupado, disponible;
    private Label lblscene1, lblscene2;
    private FlowPane pane1, pane2;
    private Scene scene1, scene2;
    private Stage thestage, newStage;
    
    private TextField fieldPatente;
    private TextField fieldRutChofer;
    
    private int numeroAnden;
    private boolean andenEstaReservado;
    private boolean andenEstaocupado;
    
    private Button boton;
    
    private Button btnEnviar;
    private TextField fieldPara;
    private TextField fieldAsunto;
    private TextArea areaMensaje;
    
    private ListView<String> listaNotificaciones;
    
    private String capturaChofer ;
    private String capturaPatente;
    private int capturaNumero;
    
    private ArrayList<Alerta> alerta;
    
    //private Timer timer;
    
    private Timeline timer;

    private int z;
    
    private Button botonHorarioParadero;
    
    private Alerta alertAux;
    
    private VistaPrueba vistaPrueba;
    
    public VistaEncargado(Scene scene,Stage primaryStage, StackPane root, String capturaUsuario){
      
        //timer = new Timer();
        
        //timer.comenzar();
        capturaNumero = -1;
        
        z = 0;
        
        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();//capta el tamaño de la pantalla del computador
        
        w = screenSize.width; //ancho de la pantalla
        h = screenSize.height; //alto de la pantalla

        
        scene.getStylesheets().add("/source/estilosEncargado.css");
        
        this.alerta = new ArrayList<Alerta>(); //arreglo para las alertas
        
        capturaChofer=""; //variable para captar Rut chofer
        capturaPatente=""; // variable para captar la patente del bus

        this.boton = new Button(); 
        
        this.setPadding(new Insets(10));
        
        //Panel que contendra el mapa
        BorderPane mapaTerminal = new BorderPane();
        
        
        int rotacion = 10;
        
        /*Panel*/
        GridPane mapaTerminalAux = new GridPane();
        mapaTerminalAux.setHgap(5);
        mapaTerminalAux.setVgap(5);
        mapaTerminalAux.setPadding(new Insets(30,0,60,0));
        mapaTerminalAux.setGridLinesVisible(false);
        mapaTerminalAux.setAlignment(Pos.CENTER);
        
        //Panel parte de arriba de la ventana
        HBox panelAndenesArriba = new HBox(5);
        panelAndenesArriba.setAlignment(Pos.CENTER);
        
        /*Botones de para los andenes del terminal*/
        anden12 = new Button("12");
        anden12.setId("andenesArriba");
        anden12.getStyleClass().add("andenDisponible");
        
        anden13 = new Button("13");
        anden13.setId("andenesArriba");
        anden13.getStyleClass().add("andenDisponible");
        
        anden14 = new Button("14");
        anden14.setId("andenesArriba");
        anden14.getStyleClass().add("andenDisponible");
        
        anden15 = new Button("15");
        anden15.setId("andenesArriba");
        anden15.getStyleClass().add("andenDisponible");
        
        anden16 = new Button("16");
        anden16.setId("andenesArriba");
        anden16.getStyleClass().add("andenDisponible");
        
        anden17 = new Button("17");
        anden17.setId("andenesArriba");
        anden17.getStyleClass().add("andenDisponible");
        
        anden18 = new Button("18");
        anden18.setId("andenesArriba");
        anden18.getStyleClass().add("andenDisponible");
        
        anden19 = new Button("19");
        anden19.setId("andenesArriba");
        anden19.getStyleClass().add("andenDisponible");
        
        anden20 = new Button("20");
        anden20.setId("andenesArriba");
        anden20.getStyleClass().add("andenDisponible");
       
        
        anden21 = new Button("21");
        anden21.setId("andenesArriba");
        anden21.getStyleClass().add("andenDisponible");
        
        anden22 = new Button("22");
        anden22.setId("andenesArriba");
        anden22.getStyleClass().add("andenDisponible");
        
        anden23 = new Button("23");
        anden23.setId("andenesArriba");
        anden23.getStyleClass().add("andenDisponible");
        
        anden24 = new Button("24");
        anden24.setId("andenesArriba");
        anden24.getStyleClass().add("andenDisponible");
    
        
        panelAndenesArriba.getChildren().addAll(anden24,anden23,anden22,anden21,anden20,anden19);
        panelAndenesArriba.getChildren().addAll(anden18,anden17,anden16,anden15,anden14,anden13,anden12);
            
        mapaTerminal.setTop(panelAndenesArriba);
              
        VBox panelAndenesIzquierda = new VBox(5);
        panelAndenesIzquierda.setAlignment(Pos.CENTER);
        
        anden25 = new Button("25");
        anden25.setId("andenIzquierda");
        anden25.getStyleClass().add("andenDisponible"); 
        
        anden26 = new Button("26");
        anden26.setId("andenIzquierda");
        anden26.getStyleClass().add("andenDisponible"); 
        
        anden27 = new Button("27");
        anden27.setId("andenIzquierda");
        anden27.getStyleClass().add("andenDisponible"); 
        
        anden28 = new Button("28");
        anden28.setId("andenIzquierda");
        anden28.getStyleClass().add("andenDisponible"); 
        
        anden29 = new Button("29");
        anden29.setId("andenIzquierda");
        anden29.getStyleClass().add("andenDisponible"); 
        
        anden30 = new Button("30");
        anden30.setId("andenIzquierda");
        anden30.getStyleClass().add("andenDisponible"); 
        
        anden31 = new Button("31");
        anden31.setId("andenIzquierda");
        anden31.getStyleClass().add("andenDisponible"); 
        
        anden32 = new Button("32");
        anden32.setId("andenIzquierda");
        anden32.getStyleClass().add("andenDisponible"); 
        
        anden33 = new Button("33");
        anden33.setId("andenIzquierda");
        anden33.getStyleClass().add("andenDisponible"); 
        
        anden34 = new Button("34");
        anden34.setId("andenIzquierda");  
        anden34.getStyleClass().add("andenDisponible"); 
        
        anden35 = new Button("35");
        anden35.setId("andenIzquierda");
        anden35.getStyleClass().add("andenDisponible"); 
        
        anden36 = new Button("36");
        anden36.setId("andenIzquierda");
        anden36.getStyleClass().add("andenDisponible"); 
        
        panelAndenesIzquierda.getChildren().addAll(anden25,anden26,anden27,anden28,anden29,anden30);
        panelAndenesIzquierda.getChildren().addAll(anden31,anden32,anden33,anden34,anden35,anden36);
        
        
        VBox panelSectorAbajo = new VBox();
        panelSectorAbajo.setAlignment(Pos.BOTTOM_RIGHT);
        Rectangle recAbajo = new Rectangle(260,100);
        recAbajo.setFill(Color.LIGHTGREY);
    
        
        HBox panelAndenesAbajo = new HBox(5);
        panelAndenesAbajo.setAlignment(Pos.BOTTOM_RIGHT);
        
        anden3 = new Button("3");
        anden3.getStyleClass().add("andenDisponible");
        anden3.setId("andenAbajo");
        anden3.setRotate(-rotacion);
        
        anden4 = new Button("4");
        anden4.getStyleClass().add("andenDisponible");
        anden4.setId("andenAbajo");
        anden4.setRotate(-rotacion);
        
        anden5 = new Button("5");
        anden5.getStyleClass().add("andenDisponible");
        anden5.setId("andenAbajo");
        anden5.setRotate(-rotacion);
        
        anden6 = new Button("6");
        anden6.getStyleClass().add("andenDisponible");
        anden6.setId("andenAbajo");
        anden6.setRotate(-rotacion);
        
        anden7 = new Button("7");
        anden7.getStyleClass().add("andenDisponible");
        anden7.setId("andenAbajo");
        anden7.setRotate(-rotacion);
        
        anden8 = new Button("8");
        anden8.getStyleClass().add("andenDisponible");
        anden8.setId("andenAbajo");
        anden8.setRotate(-rotacion);
        
        anden9 = new Button("9");
        anden9.getStyleClass().add("andenDisponible");
        anden9.setId("andenAbajo");
        anden9.setRotate(-rotacion);
        
        anden10 = new Button("10");
        anden10.getStyleClass().add("andenDisponible");
        anden10.setId("andenAbajo");
        anden10.setRotate(-rotacion);
        
        anden11 = new Button("11");
        anden11.getStyleClass().add("andenDisponible");
        anden11.setId("andenAbajo");
        anden11.setRotate(-rotacion);
        
        panelAndenesAbajo.getChildren().addAll(anden3,anden4,anden5,anden6,anden7,anden8);
        panelAndenesAbajo.getChildren().addAll(anden9,anden10,anden11);
        
        panelSectorAbajo.getChildren().addAll(panelAndenesAbajo,recAbajo);
        

        Rectangle rec1 = new Rectangle(50,50,Color.LIGHTGREY);
        
        mapaTerminalAux.add(rec1,0,0);
        mapaTerminalAux.add(panelAndenesArriba, 1, 0);
        mapaTerminalAux.add(panelAndenesIzquierda, 0, 1);
        mapaTerminalAux.add(panelSectorAbajo, 1, 1);
        
        mapaTerminalAux.setStyle("-fx-background-color:#E0F2F7;-fx-padding:10px;");
        
        HBox panelSimbologia = new HBox(10);
        panelSimbologia.setAlignment(Pos.CENTER);
        
        Rectangle recDisponible = new Rectangle(20, 20, Color.web("#009900"));
        Text textoDisponible = new Text("Disponible");
        
        Rectangle recOcupado = new Rectangle(20, 20, Color.web("#ff0000"));
        Text textoOcupado = new Text("Ocupado");
        
        Rectangle recReservado = new Rectangle(20,20,Color.web("#ff9900"));
        Text textoReservado = new Text("Reservado");
        
        panelSimbologia.getChildren().addAll(recDisponible,textoDisponible,recOcupado,textoOcupado,recReservado,textoReservado);
                
        mapaTerminalAux.add(panelSimbologia, 1,2);
        
        //Panel Derecha
        VBox panelDerecha = new VBox(30);
        panelDerecha.setPadding(new Insets(10));
        
        // variable de alto de la lista y el area de texto
        double alto = 150;
        
        // Panel notificaciones
        VBox panelNotificaciones = new VBox(10);
        panelNotificaciones.setAlignment(Pos.CENTER);
        Text textoNotificaciones = new Text("Notificaciones");
        
        //Arreglo de las notificaciones
        listaNotificaciones = new ListView<>(); 
        listaNotificaciones.setPrefHeight(alto);
        
        /*conecta a la BD*/
        conexion.ConexionBD conexion = new ConexionBD();
        
        panelNotificaciones.getChildren().addAll(textoNotificaciones,listaNotificaciones);
        
        // Panel Correo
        VBox panelCorreo = new VBox(10);
        panelCorreo.setAlignment(Pos.BOTTOM_CENTER);
       
        //Mensaje para el correo
        Text textoMensaje = new Text("Mensajes");
        // Panel para
        VBox panelPara = new VBox();
        Text textoPara = new Text("Para");
        fieldPara = new TextField();
        fieldPara.setPromptText("Para");
        panelPara.getChildren().addAll(textoPara,fieldPara);
        
        //Panel asunto
        VBox panelAsunto = new VBox();
        Text textoAsunto = new Text("Asunto");
        fieldAsunto = new TextField();
        fieldAsunto.setPromptText("Asunto");
        panelAsunto.getChildren().addAll(textoAsunto,fieldAsunto);
        
        // Mensaje
        areaMensaje = new TextArea();
        areaMensaje.setPrefSize(150, alto);
        
        // Panel Botones
        HBox panelBotonesCorreo = new HBox(10);
        panelBotonesCorreo.setAlignment(Pos.CENTER_RIGHT);
        btnEnviar = new Button("Enviar"); //Boton para enviar correo
        Button btnCancelar = new Button("Cancelar");
        panelBotonesCorreo.getChildren().addAll(btnEnviar,btnCancelar);
    
        panelCorreo.getChildren().addAll(textoMensaje,panelPara,panelAsunto,areaMensaje,panelBotonesCorreo);
                    
        // Agregar contenido al panel de la derecha
        panelDerecha.getChildren().addAll(panelNotificaciones,panelCorreo);
        panelDerecha.setStyle("-fx-background-color:#CECEF6;-fx-padding:10px;");
        
        
        EnviarCorreo("","","");
        
        // panel usuario
        HBox panelArriba = new HBox();
        panelArriba.setAlignment(Pos.CENTER_RIGHT);
        PanelUsuario panelUsuario = new PanelUsuario("Encargado: ", capturaUsuario);
        panelArriba.getChildren().add(panelUsuario);
        panelArriba.setStyle("-fx-background-color:#81F7BE;-fx-padding:10px;");
        
        
        // evento del link
        Hyperlink linkSalir = panelUsuario.linkSalir();
        linkSalir.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                scene.setRoot(root);
                Stage escena = new Stage();
                //escena.setScene(scene);
                //escena.setFullScreen(true);
                //primaryStage.setWidth(500);
                //primaryStage.setHeight(500);
                
                primaryStage.setScene(scene);
                primaryStage.setFullScreen(true);
            }
        });
        
        //Boton para horarios de los paraderos
        botonHorarioParadero = new Button();
        botonHorarioParadero.setText("Horarios Paraderos");
        botonHorarioParadero.setPrefSize(150, 50);
        alertAux = new Alerta(0,"","","","","");
        vistaPrueba = new VistaPrueba(conexion,listaNotificaciones);
        botonHorarioParadero.setOnAction(new EventHandler<ActionEvent>() {
        
            @Override
            public void handle(ActionEvent event) {
                vistaPrueba.show(); //abre ventana VistaPrueba
                
            }
        });

        HBox panelParaderos = new HBox();
        panelParaderos.getChildren().addAll(botonHorarioParadero);
        panelParaderos.setStyle("-fx-background-color:#81F7BE;-fx-padding:10px;");
        
        //Posiciona los paneles        
        this.setCenter(mapaTerminalAux);
        this.setRight(panelDerecha);
        this.setTop(panelArriba);
        this.setLeft(panelParaderos);
        
        conexion = new ConexionBD();
        consultaAnden = new ConsultaAnden(conexion.obtenerConexion());        
       
        anden = new ArrayList<Anden>();
        
        for (int i = 0; i < 37; i++)
        {
            Anden andenaAux = new Anden();
            anden.add(andenaAux);
        }
        
        //Funcion para captar los mensajes de la notificaciones
        ClickNotificaciones();
        
        //Metodo para abrir venta para Reservar, Ocupar y desocupar Anden
        ventanaEmergente();
        
        //Metodo para ir captando el tiempo transcurrido
        Timer();    
    }

    /*Metodo Para devolver el ancho de la ventana*/
    public double getW() {
        return w;
    }

    /*Metodo Para devolver el alto de la ventana*/
    public double getH() {
        return h;
    }
    
    /*Metodod para eliminar datos del Correo*/
    private void borrarContenidoPanelCorreo(TextField fieldPara, TextField fieldAsunto, TextArea areaMensaje){
        fieldPara.setText("");
        fieldAsunto.setText("");
        areaMensaje.setText("");
    }
    
    /*Metodo para abrir ventana para reservar, ocupar y desocupar anden con bus*/
    public void ventanaEmergente()
    {
        cancelar=new Button("Cancelar");
        
        lblscene2=new Label("Rut Chofer: ");
        
        fieldRutChofer = new TextField();

        fieldRutChofer.setPromptText("Rut Chofer");
        
        Label lblscene3=new Label("Patente Bus: ");
        
        fieldPatente = new TextField();
        fieldPatente.setPromptText("Patente Bus");
        
        HBox panelEstados = new HBox(10);
        
        reservado = new Button("Reservado");
        reservado.getStyleClass().add("andenReservado");
        
        ocupado = new Button("Ocupado");
        ocupado.getStyleClass().add("andenOcupado");
        
        disponible = new Button("Disponible");
        disponible.getStyleClass().add("andenDisponible");
        
        panelEstados.getChildren().addAll(reservado, ocupado, disponible);
        
        HBox panelConfirmar= new HBox(10);
        
        panelConfirmar.getChildren().addAll(cancelar);
        
        pane2=new FlowPane();
        pane2.setVgap(20);
        
        pane2.setStyle("-fx-background-color:#81F79F;-fx-padding:10px;");
        pane2.getChildren().addAll(lblscene2, fieldRutChofer, lblscene3, fieldPatente, panelEstados, panelConfirmar);
         
        scene2 = new Scene(pane2, 240, 180);
         
        newStage = new Stage();
        newStage.setScene(scene2);
        newStage.initModality(Modality.APPLICATION_MODAL);
        newStage.setTitle("Pop up window");
        
        /*Evento para los botones de cada anden para desplegar ventana emergente*/
        anden3.setOnAction(e-> ButtonClicked(e));
        anden4.setOnAction(e-> ButtonClicked(e));
        anden5.setOnAction(e-> ButtonClicked(e));
        anden6.setOnAction(e-> ButtonClicked(e));
        anden7.setOnAction(e-> ButtonClicked(e));
        anden8.setOnAction(e-> ButtonClicked(e));
        anden9.setOnAction(e-> ButtonClicked(e));
        anden10.setOnAction(e-> ButtonClicked(e));
        anden11.setOnAction(e-> ButtonClicked(e));
        anden12.setOnAction(e-> ButtonClicked(e));
        anden13.setOnAction(e-> ButtonClicked(e));
        anden14.setOnAction(e-> ButtonClicked(e));
        anden15.setOnAction(e-> ButtonClicked(e));
        anden16.setOnAction(e-> ButtonClicked(e));
        anden17.setOnAction(e-> ButtonClicked(e));
        anden18.setOnAction(e-> ButtonClicked(e));
        anden19.setOnAction(e-> ButtonClicked(e));
        anden20.setOnAction(e-> ButtonClicked(e));
        anden21.setOnAction(e-> ButtonClicked(e));
        anden22.setOnAction(e-> ButtonClicked(e));
        anden23.setOnAction(e-> ButtonClicked(e));
        anden24.setOnAction(e-> ButtonClicked(e));
        anden25.setOnAction(e-> ButtonClicked(e));
        anden26.setOnAction(e-> ButtonClicked(e));
        anden27.setOnAction(e-> ButtonClicked(e));
        anden28.setOnAction(e-> ButtonClicked(e));
        anden29.setOnAction(e-> ButtonClicked(e));
        anden30.setOnAction(e-> ButtonClicked(e));
        anden31.setOnAction(e-> ButtonClicked(e));
        anden32.setOnAction(e-> ButtonClicked(e));
        anden33.setOnAction(e-> ButtonClicked(e));
        anden34.setOnAction(e-> ButtonClicked(e));
        anden35.setOnAction(e-> ButtonClicked(e));
        anden36.setOnAction(e-> ButtonClicked(e));
        
        cancelar.setOnAction(e-> ButtonClicked(e));
        
        /*Botones para captar las acciones de los botones de reservado, ocupado y disponible*/
        reservado.setOnAction(e-> ButtonClicked(e));
        ocupado.setOnAction(e-> ButtonClicked(e));
        disponible.setOnAction(e-> ButtonClicked(e));  
    }
    
    /*Metodo para Seleccionar el numero del anden y dejar el anden reservado y ocupado al hacer click en el boton del anden*/
    private void seleccionaAndenes(int numeroAnden)
    {
            this.numeroAnden = numeroAnden;
            andenEstaReservado = anden.get(this.numeroAnden).getReservado();
            andenEstaocupado = anden.get(this.numeroAnden).getOcupado();
            newStage.showAndWait(); //abre ventana emergente  
    }
    
    /*Metodo para captar los eventos de los botones*/
    private void ButtonClicked(ActionEvent e)
    {
        //conecta a la BD
        conexion.ConexionBD conexion = new ConexionBD();
       
        //CRUD Bus de la BD
        ConsultaBus bus = new ConsultaBus(conexion.obtenerConexion());
        //CRUD chofer de la BD
        ConsultaChofer chofer = new ConsultaChofer(conexion.obtenerConexion());
        
        /*Capta si se ha hecho click en botones*/
        if (e.getSource()==anden3)
        {
           boton = anden3;
           seleccionaAndenes(3); 
        }
        if (e.getSource()==anden4)
        {
            boton = anden4;
            seleccionaAndenes(4);
        }
        if (e.getSource()==anden5)
        {
            boton = anden5;
            seleccionaAndenes(5);          
        }
        if (e.getSource()==anden6)
        {
            boton = anden6;
            seleccionaAndenes(6);          
        }
        if (e.getSource()==anden7)
        {
            boton = anden7;
            seleccionaAndenes(7);          
        }
        if (e.getSource()==anden8)
        {
            boton = anden8;
            seleccionaAndenes(8);          
        }
        if (e.getSource()==anden9)
        {
            boton = anden9;
            seleccionaAndenes(9);          
        }
        if (e.getSource()==anden10)
        {
            boton = anden10;
            seleccionaAndenes(10);          
        }
        if (e.getSource()==anden11)
        {
            boton = anden11;
            seleccionaAndenes(11);          
        }
        if (e.getSource()==anden12)
        {
            boton = anden12;
            seleccionaAndenes(12);          
        }
        if (e.getSource()==anden13)
        {
            boton = anden13;
            seleccionaAndenes(13);          
        }
        if (e.getSource()==anden14)
        {
            boton = anden14;
            seleccionaAndenes(14);          
        }
        if (e.getSource()==anden15)
        {
            boton = anden15;
            seleccionaAndenes(15);          
        }
        if (e.getSource()==anden16)
        {
            boton = anden16;
            seleccionaAndenes(16);          
        }
        if (e.getSource()==anden17)
        {
            boton = anden17;
            seleccionaAndenes(17);          
        }
        if (e.getSource()==anden18)
        {
            boton = anden18;
            seleccionaAndenes(18);          
        }
        if (e.getSource()==anden19)
        {
            boton = anden19;
            seleccionaAndenes(19);          
        }
        if (e.getSource()==anden20)
        {
            boton = anden20;
            seleccionaAndenes(20);          
        }
        if (e.getSource()==anden21)
        {
            boton = anden21;
            seleccionaAndenes(21);          
        }
        if (e.getSource()==anden22)
        {
            boton = anden22;
            seleccionaAndenes(22);          
        }
        if (e.getSource()==anden23)
        {
            boton = anden23;
            seleccionaAndenes(23);          
        }
        if (e.getSource()==anden24)
        {
            boton = anden24;
            seleccionaAndenes(24);          
        }     
        if (e.getSource()==anden25)
        {
           boton = anden25;
           seleccionaAndenes(25);    
        }
        if (e.getSource()==anden26)
        {
           boton = anden26;
           seleccionaAndenes(26);          
        }
        if (e.getSource()==anden27)
        {
            boton = anden27;
            seleccionaAndenes(27);          
        }
        if (e.getSource()==anden28)
        {
           boton = anden28;
           seleccionaAndenes(28); 
        }
        if (e.getSource()==anden29)
        {
           boton = anden29;
           seleccionaAndenes(29);          
        }
        if (e.getSource()==anden30)
        {
            boton = anden30;
            seleccionaAndenes(30);          
        }
        if (e.getSource()==anden31)
        {
            boton = anden31;
            seleccionaAndenes(31);          
        }
        if (e.getSource()==anden32)
        {
            boton = anden32;
            seleccionaAndenes(32);          
        }
        if (e.getSource()==anden33)
        {
            boton = anden33;
            seleccionaAndenes(33);          
        }
        if (e.getSource()==anden34)
        {
            boton = anden34;
            seleccionaAndenes(34);          
        }
        if (e.getSource()==anden35)
        {
            boton = anden35;
            seleccionaAndenes(35);          
        }
        if (e.getSource()==anden36)
        {
            boton = anden36;
            seleccionaAndenes(36);          
        }

        if(e.getSource()==reservado)//Se reserva anden
        {
            //si es la primer vez que se entra al programa y no se ha captado numero de la lista de notoficaciones no puede reservarse anden
            if(capturaNumero>-1)
            {
                int yaEstaBusEnAnden = 0;

                for(int i=0; i<anden.size() ; i++)//se comprueba que no este el bus ingresado en un anden
                {
                    if(alerta.get(capturaNumero).getBus().equals(anden.get(i).getPatente()))
                    {
                        yaEstaBusEnAnden++;
                    }
                }

                if(andenEstaReservado == false && andenEstaocupado == false && capturaNumero>-1 && yaEstaBusEnAnden == 0 && alerta.get(capturaNumero).getId()==2)
                {
                    String RutChofer = chofer.getChofer(capturaChofer);

                    String patente = bus.getPatente(capturaPatente);

                    /*Se capta fecha actual*/
                    Date d = new Date();  
                    SimpleDateFormat plantilla = new SimpleDateFormat("dd/MM/yyyy H:mm:ss");
                    String fecha = plantilla.format(d);

                    if(!RutChofer.equals("-1")) //si encontro en la BD rut de chofer continua
                    {
                        if(!patente.equals("-1"))//si encontro en la BD patente de bus continua
                        {

                            anden.get(numeroAnden).setRut(RutChofer);//se traspasa a anden el rut del chofer que se le reserva dicho anden

                            anden.get(numeroAnden).setPatente(patente);//se traspasa a anden la patente del bus que se le reserva dicho anden

                            anden.get(numeroAnden).setNumero(numeroAnden); //se traspasa el numero del anden que se reservo

                            anden.get(numeroAnden).setReservado(true);//se reserva anden

                            consultaAnden.insertarAnden(RutChofer, patente, numeroAnden, fecha, "reservado"); //ingresa los datos a la BD

                            boton.getStyleClass().set(1, "andenReservado");//cambia de color  a  amarillo el anden

                            /*CRUD busTieneAlerta*/
                            ConsultaBusTieneAlerta busTieneAlerta = new ConsultaBusTieneAlerta(conexion.obtenerConexion());
                            busTieneAlerta.insertarBusTieneAlert(patente, 2, fecha);//se traspasa patente id y fecha a la tabla busTieneAlerta

                            newStage.close(); //cierra ventana

                            //anden.get(numeroAnden).imprimirAnden();
                            String and = String.valueOf(numeroAnden); //pasa el numero del anden a un string
                            String asunto = "Anden "+ and;
                            String mensaje = "Se le reservo anden "+and;
                             //se traspasa al MetodoCorreoChofer para enviar correo al chofer dando aviso que se le reservo anden
                            enviarCorreoAChofer(RutChofer, asunto, mensaje);

                            listaNotificaciones.getItems().remove(capturaNumero);//elimina de la lista de notifiaciones en dicha posicion
                            alerta.remove(capturaNumero); //elimina del arreglo alerta la alerta en dicha posicion

                            capturaNumero = -1;

                        }
                        else
                            System.out.println("No existe patente");      
                    }          

                    else
                        System.out.println("No existe RutChofer");           
                }
            }
        }
        if(e.getSource() == ocupado)//Dejar Aden ocupado
        {
            if(andenEstaReservado == true)//esta reservado listo para ocuparse el anden
            { 
                /*Capta fecha actual*/
                Date d = new Date();  
                SimpleDateFormat plantilla = new SimpleDateFormat("dd/MM/yyyy H:mm:ss");
                String fecha = plantilla.format(d);                
                
                String RutChofer=anden.get(numeroAnden).getRut();

                String patente = anden.get(numeroAnden).getPatente();

                anden.get(numeroAnden).setFecha(fecha);

                anden.get(numeroAnden).setReservado(false); // ya no esta reservado, se ocupa
                anden.get(numeroAnden).setOcupado(true);//se ocupa anden

                consultaAnden.insertarAnden(RutChofer, patente, numeroAnden, fecha, "ocupado"); //ingresa los datos a la BD

                boton.getStyleClass().set(1, "andenOcupado");//cambia de color a rojo el anden
                                
                for (int i = 0; i < alerta.size(); i++)
                {
                    if (anden.get(numeroAnden).getPatente().equals(alerta.get(i).getBus()))//si en la busqueda encuentra la patente la elimina
                    {
                        alerta.remove(i);//elimina del arreglo de alertas
                        listaNotificaciones.getItems().remove(i);//elimina de la lista de alertas
                        i--;
                    }
                    
                }
                
                newStage.close(); //cierra ventana                
            }
                
        }
        if(e.getSource() == disponible)//Dejar anden diposnible
        {
            if(andenEstaocupado == true)
            {
                //Pasar a la BD hora en la que fue desocupado 
                anden.get(numeroAnden).setOcupado(false);//se desocupa anden
                
                /*Capta la fecha actual*/
                Date d = new Date();  
                SimpleDateFormat plantilla = new SimpleDateFormat("dd/MM/yyyy H:mm:ss");
                String fecha = plantilla.format(d);                
                
                String RutChofer=anden.get(numeroAnden).getRut();

                String patente = anden.get(numeroAnden).getPatente();

                consultaAnden.insertarAnden(RutChofer, patente, numeroAnden, fecha, "disponible"); //ingresa los datos a la BD
                
                boton.getStyleClass().set(1, "andenDisponible");//cambia de color a rojo el anden
                
                for (int i = 0; i < alerta.size(); i++)//busca en el arreglo alerta para eliminar
                {

                    if (anden.get(numeroAnden).getPatente().equals(alerta.get(i).getBus()))//si en la busqueda encuentra la patente la elimina
                    {
                        alerta.remove(i);//elimina del arreglo de alertas
                        listaNotificaciones.getItems().remove(i);//elimina de la lista de alertas
                        i--;
                    }
                    
                }
                
                anden.get(numeroAnden).setFecha("");
                anden.get(numeroAnden).setOcupado(false);//se desocupa anden
                anden.get(numeroAnden).setRut("");//se vacia rut chofer del anden
                anden.get(numeroAnden).setPatente("");//se vacia patente del bus del anden
                
                newStage.close(); //cierra ventana
            }
                
        }
        else if(e.getSource()==cancelar)
            newStage.close();
    }
    
    /*Metodo para captar la notificación al hacerle click*/
    public void ClickNotificaciones()   //captura el string de la lista de avisos al hacer click sobre el aviso         
    {
        
        try{
            listaNotificaciones.setOnMouseClicked((MouseEvent event) ->
            {
                capturaChofer="";
                capturaPatente="";
                int cont = 0;
                String stringLista  = listaNotificaciones.getSelectionModel().getSelectedItem();//captura el string de la lista de avisos
                //Capta el numero de la posicion que se hizo click en la lista de la notificaciones
                capturaNumero = listaNotificaciones.getSelectionModel().getSelectedIndex();

                if(capturaNumero>-1)//Si no hay nada en las notificaiones no pasa
                {
                    capturaChofer = alerta.get(capturaNumero).getConductor();
                    capturaPatente = alerta.get(capturaNumero).getBus();

                    String asunto = alerta.get(capturaNumero).getNombre();
                    String mensaje = alerta.get(capturaNumero).getDescripcion();

                    /*Metodo para capturar datos para enviar correo*/
                    enviarCorreoAChofer(capturaChofer, asunto, mensaje);

                    fieldRutChofer.setPromptText(capturaChofer);
                    fieldPatente.setPromptText(capturaPatente);
                }
            });
        }
        catch(Exception e)
        {
            System.err.println("Ha ocurrido un error!");
            System.err.println(e.getMessage());
        }
    }
    
    public void RevisaHoraParaMultar()//cuando bus llega a anden empieza el conteo y ver si se esta atrasando
    {
        Date d = new Date();  
        SimpleDateFormat plantilla = new SimpleDateFormat("dd/MM/yyyy H:mm:ss");
        String fechaActual = plantilla.format(d);

        Date a = new Date(fechaActual); //fecha actual
        
        for(int i=0; i<anden.size();i++)
        {
            if(anden.get(i).getFecha()!="")
            {
                Date b = new Date(anden.get(i).getFecha());//Fecha cuando ingreso bus al anden

                long diferencia = (Math.abs(a.getTime() - b.getTime())) / 1000;
                diferencia = diferencia /60;//calcula los segundos para pasarlo a minutos           
            
                if(diferencia  == 1)//LA DIFERENCIA DEBE SER 13 MIN, para avisar al chofer que se esta pasando del tiempo en el anden
                {
                    System.out.println("Bus del anden "+anden.get(i).getNumero()+" te estas pasando de los minutos, va a ver multa");
                    if(((Math.abs(a.getTime() - b.getTime())) / 1000)%60 ==0)//resto para que sea una vez que ingrese a a lista de alerta
                    {
                        ingresaAlertas(4, anden.get(i).getPatente(), anden.get(i).getRut());//ingresa dato al arreglo alertas

                        ingresarDatoListaAlerta(alerta.size()-1); //ingresa datos a lista de alertas
                    }
                }
                if(diferencia  == 2)//LA DIFERENCIA DEBE SER 15 MIN (Ingresar dato a BD tabla busienealerta), para multar
                {
                    
                    System.out.println("Bus del anden "+anden.get(i).getNumero()+" te pasaste, va multa");
                    
                    if(((Math.abs(a.getTime() - b.getTime())) / 1000)%60 ==0)//resto para que sea una vez que ingrese a a lista de alerta
                    {                    
                        ingresaAlertas(1, anden.get(i).getPatente(), anden.get(i).getRut());//ingresa dato al arreglo alertas

                        ingresarDatoListaAlerta(alerta.size()-1);//ingresa datos a lista de alertas
                    }
                }
            }
        }
    }
    
    /*Creo que este metodo esta demas pero me da miedo borrarlo*/
    public void MultarBus(int numeroAnden)//cuando bus se va del anden
    {
        Date d = new Date();  
        SimpleDateFormat plantilla = new SimpleDateFormat("dd/MM/yyyy H:mm:ss");
        String fechaActual = plantilla.format(d);
        
        conexion.ConexionBD conexion = new ConexionBD();
       
        ConsultaBus bus = new ConsultaBus(conexion.obtenerConexion());
        
        Date a = new Date(fechaActual);
        Date b = new Date(anden.get(numeroAnden).getFecha());      
        long diferencia = (Math.abs(a.getTime() - b.getTime())) / 1000;
        diferencia = diferencia /60;
        System.out.println("diferencia: "+diferencia);
        String multaBus="Bus ";
        
        if(diferencia == 2)//LA DIFERENCIA DEBE SER 15 MIN (Ingresar dato a BD tabla busienealerta
        {
            System.out.println("Hay multa");
            
            bus.modificarBus2(anden.get(numeroAnden).getPatente(), 10000);
            
            multaBus=multaBus+anden.get(numeroAnden).getPatente();
            multaBus=multaBus+" en anden ";
            multaBus=multaBus+numeroAnden;
            multaBus=multaBus+" tiene multa de $10.000";
            listaNotificaciones.getItems().add(multaBus);
            //anden.get(numeroAnden).set
        }
    }
    
    /*Metodo para ingresar los datos a las notificaciones*/
    public void ingresarDatoListaAlerta(int i)
    {
        //String chofer = this.alerta.get(i).getConductor();
        String patente = this.alerta.get(i).getBus();
        String descripcion = this.alerta.get(i).getDescripcion();
        listaNotificaciones.getItems().add("Bus "+patente+"  "+descripcion);//se agrega información a lista de notificaciones
        //listaNotificaciones.getItems().add(chofer);
    }
    
    /*Metodo para ingresar datos al arrayList alerta*/
    public void ingresaAlertas(int idAlerta, String patente, String chofer)
    {
        //Captura fecha actual
        Date d = new Date();  
        SimpleDateFormat plantilla = new SimpleDateFormat("dd/MM/yyyy H:mm:ss");
        String fechaActual = plantilla.format(d);
        
        //conecta a la BD
        conexion.ConexionBD conexion = new ConexionBD();
        
        //CRUD tabla alerta
        ConsultaAlerta consultaAlerta = new ConsultaAlerta(conexion.obtenerConexion());
        
        String nombreAlerta = consultaAlerta.getNombre(idAlerta);//capta id de la alerta
        String descripcionAlerta = consultaAlerta.getDescricpion(idAlerta);//capta descripcion de la alerta

        Alerta alertaAux = new Alerta(idAlerta, nombreAlerta, descripcionAlerta, patente, chofer, fechaActual);//se crea nueva clase alerta
        
        alerta.add(alertaAux);//Se agrega nueva alerta al arrayList alerta
    }
    
    /*Metodo para captar los datos del chofer y poder enviar correo al Chofer*/
    public void enviarCorreoAChofer(String RutChofer, String asunto, String mensaje)
    {
        conexion.ConexionBD conexion = new ConexionBD();
       
        ConsultaChofer consultaChofer = new ConsultaChofer(conexion.obtenerConexion());
        
        String correo = consultaChofer.getCorreo(RutChofer);
        
        EnviarCorreo(correo, asunto, mensaje);//se envia correo con todos los datos con este metodo
    }
    
    /*Metodo para enviar correo al chofer*/
    public void EnviarCorreo(String correo2, String asunto2, String mensaje2)
    {
        fieldPara.setText(correo2);
        fieldAsunto.setText(asunto2);
        areaMensaje.setText(mensaje2);
        
        //evento enviar correo
        btnEnviar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                String destino = fieldPara.getText();
                String asunto = fieldAsunto.getText();
                String mensaje = areaMensaje.getText();
                
                Correo correo = new Correo();
                correo.setDestino(destino);//pasa el correo del chofer
                correo.setAsunto(asunto);//pasa el asunto del correo
                correo.setMensaje(mensaje);//pasa el mensaje a enviar al correo
                
                ControladorCorreo controladorCorreo = new ControladorCorreo();
                boolean enviado = controladorCorreo.envioCorreo(correo);//Metodo donde envia correo
                if (enviado)//si se puedo enviar correo
                {
                    System.out.println("Correo enviado con exito!");

                    borrarContenidoPanelCorreo(fieldPara,fieldAsunto,areaMensaje);//borra el contenido del correo

                }
                else{
                    System.out.println("Correo no enviado :(");
                }
                
            }
        });
    }
    
    /*Metodo para contar el tiempo que trasncurre*/
    public void Timer()
    {
         EventHandler<ActionEvent> evento = new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("Tiempo transcurrido: "+z+" segundos");
                z++;
                RevisaHoraParaMultar();//cada segundo va revisando si en algun anden hay un bus que ya se paso de los minutos y se multa
                
                ingresarDesdeVistaPrueba();//Ingresa lo datos a la notificacion desde la clase de recorrido de los buses (vistaPrueba)
            }
            
        }; 
        
        KeyFrame kf = new KeyFrame(Duration.seconds(1),evento);
        this.timer = new Timeline(kf);
        this.timer.setCycleCount(Timeline.INDEFINITE);
        this.timer.play();
                

    }
    
    /*Metodo para ingresar los datos a la lista de notificación y al arreglo de alertas desde la clase de recorrido de los buses (vistaPrueba)*/
    public void ingresarDesdeVistaPrueba()
    {
        vistaPrueba.getCambio();
        
        //si se realizo algun cambio en vistaPrueba entonces se ingresan los datos a la notificacion y al arreglo alerta
        if(vistaPrueba.getCambio()==true)
        {
            alertAux=vistaPrueba.getAlerta();//capta clase alerta desde vistaPrueba
            ingresaAlertas(alertAux.getId(),alertAux.getBus(),alertAux.getConductor());//se ingresa alerta capta desde vistaPrueba al arreglo alerta
            ingresarDatoListaAlerta(alerta.size()-1);//Se ingresan la información a la notificación
            vistaPrueba.setCambio(false);//ya no hay cambios en calse vistaPrueba
        }
    }
}