/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import javafx.scene.control.Hyperlink;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 *
 * @author Usuario
 */
public class PanelUsuario extends GridPane{
    
    private Text textoTipoUsuario;
    private Text textoUsuario;
    private Hyperlink linkSalir;
    Text palo=new Text(" | ");
    
    public PanelUsuario(String tipoUsuario, String usuario){
        this.setGridLinesVisible(false);
        this.setStyle("-fx-border-color:black;\n-fx-border-width:0;\n-fx-padding: 10;\n-fx-background-color:#b3cbff;");
        int espacio = 5;
        
        this.setHgap(espacio);
        this.setVgap(espacio);
        
        int sizeFont = 12;
        
        textoTipoUsuario = new Text(tipoUsuario+": ");
        textoTipoUsuario.setFont(Font.font("Arial", FontWeight.BOLD, sizeFont));
        
        textoUsuario = new Text(usuario);
        textoUsuario.setFont(new Font(sizeFont));
        
        linkSalir = new Hyperlink("Salir");
        linkSalir.setFont(new Font(sizeFont));
        linkSalir.setStyle("-fx-text-fill:blue;");
        
        this.add(textoTipoUsuario, 0, 0);
        this.add(textoUsuario,1,0);
        this.add(palo,2,0);
        this.add(linkSalir, 3, 0);
    }
    
    public Hyperlink linkSalir(){
        return linkSalir;
    }
    
}
