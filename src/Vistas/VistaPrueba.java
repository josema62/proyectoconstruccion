/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Controlador.Alerta;
import Controlador.ControladorHorario;
import ModeloDatos.Bus;
import ModeloDatos.Paradero;
import ModeloDatos.Ruta;
import ModeloDatos.SalidaAux;
import conexion.ConexionBD;
import conexion.ConsultaAlerta;
import conexion.ConsultaBus;
import conexion.ConsultaHora;
import conexion.ConsultaSalidaAux;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Gustavo Rojas
 */
public class VistaPrueba extends Stage{
    /* Ancho de la ventana */ 
    private double w = 500;
    /* Alto de la ventana */
    private double h = 600;
    /* Cadena que guarda la patente del bus
    seleccionado en el combo box*/
    private String patenteBus;
    /* Cadena que guarda el rut del conductor de un bus */
    private String rutConductor;
    
    /* Objeto que crea una conexion a la base de daatos */
    private Connection con;
    /* Lista de los radios botones */
    private ArrayList<RadioButton> radios; 
    /* Lista de los horarios a mostrar */
    private ArrayList<String> horarios;
    /* Lista de los paraderos a mostrar */
    private ArrayList<Paradero> paraderos;
    
    /* Objeto que guarda informacion de la salida del terminal */
    private Paradero paraderoSalida;
    /* Objeto que guarda informacion de la entrada del terminal */
    private Paradero paraderoEntrada;
    
    /* Objeto que guarda la salida seleccionada en el combobox */
    private SalidaAux salidaBus;
    /* Objeto que guarda las salidas que se cargaron en el combobox */
    private ArrayList<SalidaAux> salidasCombo;
    
    private ListView<String> listaNotificaciones;
    
    private Alerta alerta;
    private boolean cambio;
    
    /**
     * Constructor de la clase VistaPrueba.Este constructor inicializa todo
     * los componentes graficos de la vista como botones, combobox y la lista
     * que guardan datos.
     * @param conexionBD Conexion a la base de datos enviada desde vistaEncargado
     * @param lista 
     */
    public VistaPrueba(ConexionBD conexionBD, ListView<String> lista){
        
        
        /* Inicializacion de informacion que sera mostrada en los componentes
        graficos */
        this.paraderoSalida = new Paradero("","Terminal(Salida)","");
        this.paraderoEntrada = new Paradero("","Terminal(Entrada)","");
        
        this.salidaBus = new SalidaAux("","","","");
        
        this.patenteBus = "";
        this.rutConductor = "";
        
        this.listaNotificaciones = lista;
        
        alerta = new Alerta(0,"","","","","");
        cambio = false;
        
        /* Obtengo la conexion */
        this.con = conexionBD.obtenerConexion();
        
        /* Seteo el ancho,alto y nombre de la ventana */
        this.setWidth(w);
        this.setHeight(h);
        this.setTitle("Ventana de prueba");
        //this.initModality(Modality.APPLICATION_MODAL);
        
        /* Inicializo las listas */
        this.horarios = new ArrayList<>();
        this.paraderos = new ArrayList<>();
        this.radios = new ArrayList<>();
        this.salidasCombo = new ArrayList<>();
        
        ConsultaBus consultaBus = new ConsultaBus(conexionBD.obtenerConexion());
        
        /* Panel principal de ventana que divide la ventana en 5 partes
           Top-Left-Bottom-Right-Center
        */
        BorderPane panelPrincipal = new BorderPane();
        panelPrincipal.setPadding(new Insets(10));
        
        VBox panelIzquierda = new VBox(10);
        
        Text textoComboBuses = new Text("Bus");
        
        /* Inicializo el combobox de los buses */
        ComboBox<String> comboBuses = new ComboBox<>();
        comboBuses.setPromptText("Selecciona un bus");
        consultaBus.mostrarTodosLosBuses();
        ArrayList<Bus> buses = consultaBus.getBuses();
        this.cargarBuses(comboBuses, buses);
        
        Text textoRetraso = new Text("Retraso");
        
        Text textoSalida = new Text("Salida");
        
        /* Inicializo el combox de las salidas */
        ComboBox<String> comboSalida = new ComboBox();
        comboSalida.setDisable(true);
        comboSalida.setPromptText("Selecciona una salida");
        
        /* Carga los tiempos de retrasos. Se cargaran desde 0 a 60 minutos*/
        ComboBox<Integer> comboRetraso = new ComboBox();
        this.cargarRetrasos(comboRetraso);
        comboRetraso.setValue(0);
        comboRetraso.setEditable(false);
        comboRetraso.setDisable(true);
        /* Inicializo el boton */
        Button btnAplicar = new Button("Aplicar");
        btnAplicar.setDisable(true);
        
        /* Panel de la parte superior */
        GridPane panelTop = new GridPane();
        panelTop.setHgap(10);
        panelTop.setVgap(10);
        panelTop.setGridLinesVisible(false);
        panelTop.setAlignment(Pos.TOP_LEFT);
        
        panelTop.add(textoComboBuses, 0, 0);
        panelTop.add(comboBuses, 1, 0);
        panelTop.add(textoSalida,0,1);
        panelTop.add(comboSalida,1,1);
        panelTop.add(textoRetraso, 0, 2);
        panelTop.add(comboRetraso, 1, 2);
        panelTop.add(btnAplicar,2,2);
        
        /* Panel de la parte centro */
        VBox panelCentro = new VBox(5);
        Text textoParaderos = new Text("Paraderos");
        textoParaderos.setFont(Font.font(Font.getDefault().getFamily(),FontWeight.BOLD, 16));
        
        GridPane panelListaParaderos = new GridPane();
        panelListaParaderos.setVgap(10);
        panelListaParaderos.setHgap(10);
        panelListaParaderos.setGridLinesVisible(false);
        
        
        /* Agrego al panel del centro un texto con el titulo de los paraderos
        y la lista de los radios buttons asociado a cada paradero*/
        panelCentro.getChildren().addAll(textoParaderos,panelListaParaderos);
        /* Agrego al panel del centro un texto con el titulo de los paraderos
        y la lista de los radios buttons asociado a cada paradero */
        panelIzquierda.getChildren().addAll(panelTop,panelCentro);

        /* Agrego al panel principal el paneltop(panel superior) */
        panelPrincipal.setLeft(panelIzquierda);
        
        
        
        
        
        
        Scene scene = new Scene(panelPrincipal,w,h);
        this.setScene(scene);
        
        ToggleGroup group = new ToggleGroup();
        
        comboBuses.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                salidasCombo.clear();
                
                comboSalida.setDisable(false);
                comboSalida.getSelectionModel().clearSelection();
                comboSalida.getItems().clear();
                panelListaParaderos.getChildren().clear(); // linea de prueba
                
                salidaBus.setId("");
                salidaBus.setFechaSalida("");
                salidaBus.setFechaEntrada("");
                
                String patente = comboBuses.getSelectionModel().getSelectedItem();
                patenteBus = patente;
                System.out.println(patenteBus);
                ConsultaBus consultaBus = new ConsultaBus(con);
                String rut = consultaBus.rutConductorBus(patenteBus);
                rutConductor = rut;
                //System.out.println(rutConductor);
                
                //cargarSalidaBus(comboSalida,patente);
                comboSalida.getItems().clear();
                ConsultaSalidaAux consultaSalidaAux = new ConsultaSalidaAux(con);
                consultaSalidaAux.salidas(patenteBus);
                
                int n = consultaSalidaAux.getSalidas().size();
                for (int i = 0; i < n; i++) {
                    SalidaAux salidaAux = consultaSalidaAux.getSalidas().get(i);
                    salidasCombo.add(salidaAux);
                    comboSalida.getItems().add(salidaAux.getFechaSalida()+"-"+salidaAux.getFechaEntrada());
                    System.out.println(salidaAux.getFechaSalida()+"-"+salidaAux.getFechaEntrada());
                }
            }
        });
        
        comboSalida.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                if(comboSalida.getSelectionModel().getSelectedItem() == null){
                    comboRetraso.setDisable(true);
                    btnAplicar.setDisable(true);
                }
                else{
                    comboRetraso.setDisable(false);
                    btnAplicar.setDisable(false);
                }
                
 
                vaciarContenido();
                panelListaParaderos.getChildren().clear();
                
                String seleccion = "";
                if (comboSalida.getSelectionModel().getSelectedItem() != null){
                    int index = comboSalida.getSelectionModel().getSelectedIndex();
                    salidaBus.setId(salidasCombo.get(index).getId());                
                    
                    seleccion = comboSalida.getSelectionModel().getSelectedItem();
                    int indexGuion = seleccion.indexOf("-");
                    String salida = seleccion.substring(0,indexGuion);
                    String entrada = seleccion.substring(indexGuion+1);

                    salidaBus.setFechaSalida(salida);
                    salidaBus.setFechaEntrada(entrada);
                    ConsultaHora consultaHorario = new ConsultaHora(con);
                    //consultaHorario.horariosBus(patenteBus, salidaBus.getFechaSalida(), salidaBus.getFechaEntrada());
                    consultaHorario.horariosBus(patenteBus, salidaBus.getId(), salidaBus.getFechaSalida(), salidaBus.getFechaEntrada());
                    paraderos = consultaHorario.getParederos();
                    horarios = consultaHorario.getHorarios();
                    
                }
                
                /*
                String seleccion = comboSalida.getSelectionModel().getSelectedItem();
                int indexGuion = seleccion.indexOf("-");
                String salida = seleccion.substring(0,indexGuion);
                String entrada = seleccion.substring(indexGuion+1);
                
                salidaBus.setFechaSalida(salida);
                salidaBus.setFechaEntrada(entrada);
                
                
                ConsultaHora consultaHorario = new ConsultaHora(con);
                consultaHorario.horariosBus(patenteBus, salidaBus.getFechaSalida(), salidaBus.getFechaEntrada());
                paraderos = consultaHorario.getParederos();
                horarios = consultaHorario.getHorarios();
                */
                //cargarHorarios(patenteBus, salida, entrada);
                
                // colocar el horario de salida
                
                int n = horarios.size();
               
                for (int i = -1; i <= n; i++) {
                    
                    Paradero paradero = null;
                    String hora = null;
                    // agregar la hora de la salida
                    if (i == -1){
                        paradero = paraderoSalida;
                        hora = salidaBus.getFechaSalida();   
                        salidaBus.setFechaSalida(hora);
                        
                    }
                    // agregar la hora de la entrada
                    else if (i == n){
                        paradero = paraderoEntrada;
                        hora = salidaBus.getFechaEntrada();
                        salidaBus.setFechaEntrada(hora);
                    }
                    // agregar la hora de los demas paraderos
                    else{
                        paradero = paraderos.get(i);
                        hora = horarios.get(i);
                    }
                    
                    RadioButton radioParadero = new RadioButton(paradero.getNombre());
                    radioParadero.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            System.out.println(radioParadero.getText());
                        }
                    });
                    radioParadero.setToggleGroup(group);
                    
                    radios.add(radioParadero);
                    
                    Text textoHorario = new Text(hora);
                    panelListaParaderos.add(radioParadero, 0, i+1);
                    panelListaParaderos.add(textoHorario, 1, i+1);
                }
                
            }
        });
       
        
        btnAplicar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                Date d = new Date();  
                SimpleDateFormat plantilla = new SimpleDateFormat("dd/MM/yyyy H:mm:ss");
                String fechaActual = plantilla.format(d);

               
                int n = radios.size();
                for (int i = 0; i < n; i++) {
                    if (radios.get(i).isSelected()){
                        ConsultaAlerta consultaAlerta = new ConsultaAlerta(con);
                        String nombreAlerta = "";
                        String descripcionAlerta = "";
                        // bus a dos paraderos del terminal
                        if (i == n - 3){
                            
                            alerta.setId(2);
                            alerta.setNombre(nombreAlerta);
                            alerta.setDescripcion(descripcionAlerta);
                            alerta.setBus(patenteBus);
                            alerta.setConductor(rutConductor);
                            alerta.setFecha(fechaActual);

                            
                            // id alerta 2
                            //nombreAlerta = consultaAlerta.getDescricpion(2);
                            //listaNotificaciones.getItems().add("Bus "+patenteBus+" "+nombreAlerta);
                            //System.out.println("Bus esta a dos paraderos de distancia");
                            cambio = true;
                        }
                        
                        // bus llego al terminal
                        else if (i == n - 1){
                            alerta.setId(3);
                            alerta.setNombre(nombreAlerta);
                            alerta.setDescripcion(descripcionAlerta);
                            alerta.setBus(patenteBus);
                            alerta.setConductor(rutConductor);
                            alerta.setFecha(fechaActual);
                            //listaNotificaciones.getItems().add("Bus "+patenteBus+" "+nombreAlerta);
                            cambio = true;

                            
                            // id alerta 3
                            //nombreAlerta = consultaAlerta.getDescricpion(3);
                            //listaNotificaciones.getItems().add("Bus "+patenteBus+" "+nombreAlerta);
                            //System.out.println("Bus llego al terminal");
                        }
                        // bus salio del terminal
                        else if(i == 0){
                            alerta.setId(5);
                            alerta.setNombre(nombreAlerta);
                            alerta.setDescripcion(descripcionAlerta);
                            alerta.setBus(patenteBus);
                            alerta.setConductor(rutConductor);
                            alerta.setFecha(fechaActual);
                            //listaNotificaciones.getItems().add("Bus "+patenteBus+" "+nombreAlerta);
                            cambio = true;

                            // id alerta 5
                            //nombreAlerta = consultaAlerta.getDescricpion(5);
                            //listaNotificaciones.getItems().add("Bus "+patenteBus+" "+nombreAlerta);
                            //System.out.println("Bus salio del terminal");
                        }
                        
                        int retraso = comboRetraso.getSelectionModel().getSelectedItem();
                        
                        ControladorHorario controladorHorario = new ControladorHorario(conexionBD,horarios,paraderos, salidaBus);
                        controladorHorario.actualizarHorariosParaderos(retraso, i);
                        
                        break;
                        
                    }
                }
                ConsultaSalidaAux consultaSalidaAux = new ConsultaSalidaAux(con);
                System.out.println("Salida id: "+salidaBus.getId());
                System.out.println("salida: "+salidaBus.getFechaSalida());
                System.out.println("Entrada; "+salidaBus.getFechaEntrada());
                consultaSalidaAux.actualizarSalida(salidaBus.getId(), salidaBus.getFechaSalida(), salidaBus.getFechaEntrada());
                //actualizarSalida(salidaBus.getId(), salidaBus.getFechaSalida(), salidaBus.getFechaEntrada());
                
                System.out.println("lalalalalalalallalala");
                int index = comboSalida.getSelectionModel().getSelectedIndex();
                comboSalida.getItems().set(index, salidaBus.getFechaSalida()+"-"+salidaBus.getFechaEntrada());
                
                int h = 0;
                n = panelListaParaderos.getChildren().size();
                for (int i = 0; i < n; i++) {
                    Node nodo = panelListaParaderos.getChildren().get(i);
                    if (nodo instanceof Text){
                        String nuevoHorario = "";
                        if (i == 1){
                            nuevoHorario = salidaBus.getFechaSalida();
                        }
                        else if (i == n - 1){
                            nuevoHorario = salidaBus.getFechaEntrada();
                        }
                        else{
                            nuevoHorario = horarios.get(h);
                            h++;   
                        }
                        Text t = (Text)nodo;
                        t.setText(nuevoHorario);
                                          
                    }
                }
                
                
                 
            }
        });
        
        /*
        comboSalida.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                comboRetraso.setDisable(false);
                btnAplicar.setDisable(false);
 
                vaciarContenido();
                panelListaParaderos.getChildren().clear();
                
                String seleccion = comboSalida.getSelectionModel().getSelectedItem();
                System.out.println(seleccion);
                int indexGuion = seleccion.indexOf("-");
                String salida = seleccion.substring(0,indexGuion);
                String entrada = seleccion.substring(indexGuion+1);
                
                ConsultaHora consultaHorario = new ConsultaHora(con);
                consultaHorario.horariosBus(patenteBus, salida, entrada);
                paraderos = consultaHorario.getParederos();
                horarios = consultaHorario.getHorarios();
                //cargarHorarios(patenteBus, salida, entrada);
                
                // colocar el horario de salida
                
                int n = horarios.size();
               
                for (int i = -1; i <= n; i++) {
                    
                    Paradero paradero = null;
                    String hora = null;
                    // agregar la hora de la salida
                    if (i == -1){
                        paradero = paraderoSalida;
                        hora = salidaBus.getFechaSalida();   
                    }
                    // agregar la hora de la entrada
                    else if (i == n){
                        paradero = paraderoEntrada;
                        hora = salidaBus.getFechaEntrada();
                    }
                    // agregar la hora de los demas paraderos
                    else{
                        paradero = paraderos.get(i);
                        hora = horarios.get(i);
                    }
                    
                    RadioButton radioParadero = new RadioButton(paradero.getNombre());
                    radioParadero.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            System.out.println(radioParadero.getText());
                        }
                    });
                    radioParadero.setToggleGroup(group);
                    
                    radios.add(radioParadero);
                    
                    Text textoHorario = new Text(hora);
                    panelListaParaderos.add(radioParadero, 0, i+1);
                    panelListaParaderos.add(textoHorario, 1, i+1);
                }
                
            }
        });
        */
        /*
        Button btnActualizar = new Button("Actualizar");
        panelIzquierda.getChildren().add(btnActualizar);
        btnActualizar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                ConsultaSalidaAux consultaSalidaAux = new ConsultaSalidaAux(con);
                consultaSalidaAux.actualizarSalida(salidaBus.getId(), salidaBus.getFechaSalida(), salidaBus.getFechaEntrada());
                //actualizarSalida(salidaBus.getId(), salidaBus.getFechaSalida(), salidaBus.getFechaEntrada());
                int h = 0;
                int n = panelListaParaderos.getChildren().size();
                for (int i = 0; i < n; i++) {
                    Node nodo = panelListaParaderos.getChildren().get(i);
                    if (nodo instanceof Text){
                        String nuevoHorario = "";
                        if (i == 1){
                            nuevoHorario = salidaBus.getFechaSalida();
                        }
                        else if (i == n - 1){
                            nuevoHorario = salidaBus.getFechaEntrada();
                        }
                        else{
                            nuevoHorario = horarios.get(h);
                            h++;   
                        }
                        Text t = (Text)nodo;
                        t.setText(nuevoHorario);
                                          
                    }
                }
            }
        });
        */
        
    }
    
    private void vaciarContenido(){
        paraderos.clear();
        horarios.clear();
        radios.clear();
    }
    
    
    private void actualizarSalida(String id, String fechaSalida, String fechaEntrada){
        String query = "{call actualizarSalida(?,?,?)}";
        CallableStatement stmt = null;
        try {
           stmt  = con.prepareCall(query);
        } catch (SQLException ex) {
            Logger.getLogger(VistaPrueba.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            stmt.setInt(1, Integer.valueOf(id));
            stmt.setString(2, fechaSalida);
            stmt.setString(3, fechaEntrada);
        } catch (SQLException ex) {
            Logger.getLogger(VistaPrueba.class.getName()).log(Level.SEVERE, null, ex);
        }
        ResultSet rs = null;
        try {
             stmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(VistaPrueba.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*
        try {
            while(rs.next()){
                int ide = Integer.valueOf(id);
                String nuevaFechaSalida = fechaSalida;
                String nuevaFechaEntrada = fechaEntrada;
                rs.updateString("fechaSalida",nuevaFechaSalida);
                rs.updateString("fechaEntrada",nuevaFechaEntrada);
            }
        } catch (SQLException ex) {
            Logger.getLogger(VistaPrueba.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
    }
    

    
    
    

    
    private void cargarRetrasos(ComboBox combo){
        int n = 12; // 15 retrasos
        for (int i = 0; i <= n; i++) {
            combo.getItems().add(5*i);
        }
    }
    
    private void cargarBuses(ComboBox combo, ArrayList<Bus> buses){
        int n = buses.size();
        Bus bus = null;
        String patente;
        for (int i = 0; i < n; i++) {
            bus = buses.get(i);
            patente = bus.getPatente();
            combo.getItems().add(patente);
        }
    }
    
    public Alerta getAlerta()
    {
        return alerta;
    }
    
    public void setCambio(boolean cambio)
    {
        this.cambio = cambio;
    }
    
    public boolean getCambio()
    {
        return this.cambio;
    }
    
    
    
    /*
    private Bus obtenerBus(ArrayList<Bus> buses, String patente){
        int n = buses.size();
        Bus bus = null;
        for (int i = 0; i < n; i++) {
            bus = buses.get(i);
            if (bus.getPatente().equals(patente)){
                return bus;
            }
        }
        return bus;
    }

    */
    
}
