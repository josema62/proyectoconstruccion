/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import conexion.ConexionBD;
import conexion.ConsultaBus;
import conexion.ConsultaEmpleadoAdministrador;
import conexion.ConsultaEncargado;
import conexion.ConsultaLogin;
import java.util.Optional;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author ovidio
 */
public class Login extends Application {
    StackPane root;
    @Override
    public void start(Stage primaryStage) {
        int posX = 100;
        int posY = 25;
        Button btnLogin = new Button();
        btnLogin.setText("Login");
        btnLogin.setPrefSize(posX, posY);
        
        Button btnCancelar = new Button();
        btnCancelar.setText("Cancelar");
        btnCancelar.setPrefSize(posX, posY);
        //btn.setAlignment(Pos.BASELINE_LEFT);

        VBox panelCenter = new VBox(70);//Panel Vertical
        panelCenter.setAlignment(Pos.BASELINE_CENTER);
        
        HBox panelBotones = new HBox(30);//Panel horizontal
        panelBotones.setAlignment(Pos.CENTER_RIGHT);
        
        Text textPanelCenter = new Text("Bienvenido al Sistema Terminal");
        textPanelCenter.setFont(new Font(20));
        
        Text textLogin = new Text("Login");
        textLogin.setFont(new Font(20));
        
        panelBotones.getChildren().addAll(btnLogin);

        //panelCenter.getChildren().addAll(textPanelCenter,panelAnden1,panelAnden2,panelAnden3);
        Text textUsuario = new Text("Usuario:");
        textUsuario.setFont(new Font(15));
        
        TextField fielUsuario = new TextField();
        fielUsuario.setPromptText("Usuario");
        
        HBox panelUsuario = new HBox(30);//Panel horizontal
        panelUsuario.setAlignment(Pos.CENTER);
        
        panelUsuario.getChildren().addAll(textUsuario, fielUsuario);
        
        Text textContraseña = new Text("Contraseña:");
        textContraseña.setFont(new Font(15));
        
        TextField fieldContrasena = new TextField();
        PasswordField passField = new PasswordField();
        passField.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                btnLogin.fire();
            }
        });
        passField.setPromptText("******");
        
        fieldContrasena.setPromptText("Contraseña");
        //Text texPrueba2 = new Text("prueba2");

        HBox panelContrasena = new HBox(30);//Panel horizontal
        panelContrasena.setAlignment(Pos.CENTER);
        
        panelContrasena.getChildren().addAll(textContraseña, fieldContrasena);
        
        VBox panelLogin = new VBox(20);//Panel horizontal
        //panelLogin.setAlignment(Pos.BASELINE_LEFT);        
        panelLogin.getChildren().addAll(panelUsuario, panelContrasena, panelBotones);

        //panelCenter.getChildren().addAll(textPanelCenter, panelLogin); pa ver algo
        // implementacion gustafox
        GridPane panelAlineado = new GridPane();
        panelAlineado.setHgap(10);
        panelAlineado.setVgap(10);
        panelAlineado.setAlignment(Pos.CENTER);
        panelAlineado.setGridLinesVisible(false);

        // colocar los components
        panelAlineado.add(textLogin, 0, 0);
        panelAlineado.add(textUsuario, 0, 1);
        panelAlineado.add(fielUsuario, 1, 1);
        panelAlineado.add(textContraseña, 0, 2);
        panelAlineado.add(passField, 1, 2);
        panelAlineado.add(panelBotones, 1, 3);
        
        panelCenter.getChildren().addAll(textPanelCenter, panelAlineado);

        /*Image image = new Image(getClass().getResourceAsStream("terminal-de-buses-de-curico.jpg")) {}; 
        Label myLabel = new Label("Texto Label"); 
        myLabel.setGraphic(new ImageView(image));*/
        root = new StackPane();
        root.getChildren().add(panelCenter);
        root.setStyle("-fx-background-color:#E0F2F7;-fx-padding:10px;");
        //root.getChildren().addAll(panelCenter); // lo comente pa ve algo    

        Scene scene = new Scene(root, 300, 400);
 

        //scene.getStylesheets().add("/source/estilos.css");
        primaryStage.setTitle("Sistema de Gestion de Terminal!!!");
        //scene.getStylesheets().addAll(this.getClass().getResource("/source/estilos.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();
        
        conexion.ConexionBD conexion = new ConexionBD();


        // acciones botones
        btnLogin.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {            
                String capturaUsuario = fielUsuario.getText();
                String password = passField.getText();
                System.out.println("nombre: " + capturaUsuario);
                System.out.println("pass: " + password);
                ConsultaLogin consultaLogin = new ConsultaLogin(conexion.obtenerConexion());
                if (consultaLogin.consultaEncargado(capturaUsuario, password)) {
                    System.out.println("capturaUsuario: " + capturaUsuario);
                    /*abre la ventana principal (terminal) del encargado*/
                    VistaEncargado vistaEncargado = new VistaEncargado(scene,primaryStage,root,capturaUsuario);
                    double w = vistaEncargado.getW();
                    double h = vistaEncargado.getH();
                    primaryStage.setWidth(w);
                    primaryStage.setHeight(h);
                    primaryStage.setFullScreen(true);
                    scene.setRoot(vistaEncargado);
                } else if (consultaLogin.consultaAdministrador(capturaUsuario, password)) {
                    String fk_empresa=consultaLogin.getFk_empresa();
                    VistaAdministrador vistaAdministrador = new VistaAdministrador(scene,conexion.obtenerConexion(),primaryStage,root,capturaUsuario,fk_empresa);
                    double w = vistaAdministrador.getW();
                    double h = vistaAdministrador.getH();
                    primaryStage.setWidth(w);
                    primaryStage.setHeight(h);
                    //primaryStage.setFullScreen(true);
                    scene.setRoot(vistaAdministrador);
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Información");
                    alert.setHeaderText(null);
                    alert.setContentText("Error en nombre de usuario y/o contraseña");
                    alert.showAndWait();
                }
                }        
            });
        
        
        btnCancelar.setOnAction(new EventHandler<ActionEvent>()//si hace click en btnCancelar se ejecuta algo
        {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Adios!");
                primaryStage.close();
            }
        });
        primaryStage.setOnCloseRequest(new EventHandler() {
            @Override
            public void handle(Event event) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Alerta");
                alert.setHeaderText("Estas a Punto de Cerrar la Aplicacion");
                alert.setContentText("Realmente Quieres Salir?");
                Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                //stage.getIcons().add(new Image("imagenes/informacion.png"));
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    Platform.exit();
                } else {
                    event.consume();
                }
            }
        });
    }

    public StackPane getRoot(){
        return root;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
