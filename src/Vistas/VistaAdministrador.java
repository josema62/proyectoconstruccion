/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import ModeloDatos.Bus;
import ModeloDatos.Conductor;
import ModeloDatos.Empleado;
import ModeloDatos.Paradero;
import ModeloDatos.Ruta;
import ModeloDatos.Salida;
import ModeloDatos.SalidaAux;
import conexion.ConsultaBus;
import conexion.ConsultaChofer;
import conexion.ConsultaEmpleadoAdministrador;
import conexion.ConsultaEmpresaTieneRuta;
import conexion.ConsultaParadero;
import conexion.ConsultaRuta;
import conexion.ConsultaRutaTieneParadero;
import conexion.ConsultaSalida;
import conexion.ConsultaSalidaAux;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 *
 * @author crist
 */
public class VistaAdministrador extends BorderPane {

    private double w = 800;
    private double h = 650;

    Button btnEmpleado, btnBus, btnRuta, btnParadero, btnSalida, btnSalir;
    Login login = new Login();
    double wbtn = 100;
    double hbtn = 35;
    TableView<String[]> table;
    ObservableList<String[]> data;
    Connection conexion;
    Text titulo;
    BorderPane subMenu;
    Stage primaryStage;
    StackPane root;
    Scene scene;
    String usuario;
    String fk_empresa;

    public VistaAdministrador(Scene scene, Connection conexion, Stage primaryStage, StackPane root, String usuario, String empresa) {
        subMenu = new BorderPane();
        this.primaryStage = primaryStage;
        this.root = root;
        this.scene = scene;
        this.conexion = conexion;
        this.usuario = usuario;
        this.fk_empresa = empresa;
        scene.getStylesheets().add("/source/estilosEncargado.css");
        this.setPadding(new Insets(1, 1, 1, 1));
        this.setStyle("-fx-background-color: #b3b3ff");
        VBox contenedorBotonesIzquierdo = new VBox(25);
        contenedorBotonesIzquierdo.setAlignment(Pos.CENTER);
        VBox contenedorBotonesIzquierdo2 = new VBox();
        contenedorBotonesIzquierdo2.setAlignment(Pos.CENTER);
        BorderPane contenedorBotonesIzquierdo3 = new BorderPane();
        contenedorBotonesIzquierdo3.setPadding(new Insets(20, 10, 80, 10));
        Text administracion = new Text("Administración Terminal Curicó");
        administracion.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        Text opciones = new Text("Opciones");
        opciones.setFont(new Font("Arial", 14));
        btnEmpleado = new Button("Empleados");
        btnEmpleado.setPrefSize(wbtn, hbtn);
        btnEmpleado.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                menuEmpleado();
            }
        });
        btnBus = new Button("Buses");
        btnBus.setPrefSize(wbtn, hbtn);
        btnBus.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                menuBus();
            }
        });
        btnRuta = new Button("Rutas");
        btnRuta.setPrefSize(wbtn, hbtn);
        btnRuta.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                VistaRutas vistaRutas = new VistaRutas(scene, conexion, primaryStage, root, usuario, empresa);
                scene.setRoot(vistaRutas);
            }
        });
        btnSalida = new Button("Salidas");
        btnSalida.setPrefSize(wbtn, hbtn);
        btnSalida.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                menuSalida();
            }
        });
        contenedorBotonesIzquierdo.getChildren().addAll(opciones, btnEmpleado, btnBus, btnRuta, btnSalida);

        contenedorBotonesIzquierdo3.setTop(administracion);
        contenedorBotonesIzquierdo3.setCenter(contenedorBotonesIzquierdo);
        contenedorBotonesIzquierdo3.setBottom(contenedorBotonesIzquierdo2);
        this.setLeft(contenedorBotonesIzquierdo3);

        subMenu.setPadding(new Insets(10, 10, 40, 10));
        subMenu.setStyle("-fx-background-color: #e6e6e6");
        VBox vbox = new VBox(20);
        Text bienvenida = new Text("Bienvenido al Sistema de Administracion de Agencia!");
        Text bienvenida2 = new Text("Pulse un boton para empezar");
        bienvenida.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        bienvenida2.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        vbox.getChildren().addAll(bienvenida, bienvenida2);
        vbox.setAlignment(Pos.CENTER);
        subMenu.setCenter(vbox);
        this.setCenter(subMenu);

    }

    BorderPane menuEmpleado() {
        PanelUsuario panelUsuario = new PanelUsuario("administrador", usuario);
        HBox panelUsuarioLogeado = new HBox();
        panelUsuarioLogeado.setAlignment(Pos.CENTER_RIGHT);
        panelUsuarioLogeado.getChildren().add(panelUsuario);
        Hyperlink linkSalir = panelUsuario.linkSalir();
        linkSalir.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                scene.setRoot(root);
                primaryStage.setWidth(500);
                primaryStage.setHeight(500);
                primaryStage.setScene(scene);
            }
        });
        titulo = new Text("Administracion de Empleados");
        titulo.setFont(Font.font("Arial", FontWeight.BOLD, 18));
        VBox panelTitulo = new VBox(50);
        VBox panelTitul2 = new VBox();
        panelTitulo.getChildren().addAll(panelUsuarioLogeado, titulo, panelTitul2);
        panelTitulo.setAlignment(Pos.CENTER);
        subMenu.setTop(panelTitulo);
        //consulta a la base de datos para llenar la tabla
        ConsultaChofer chofer = new ConsultaChofer(getConexion());
        chofer.mostrarChofer(this.fk_empresa);
        updateTableEmpleado(tablaEmpleado(chofer.getConductores()));
        //-----------------------------
        subMenu.setCenter(table);
        this.setCenter(subMenu);
        //agregando crud
        HBox panelCrud = new HBox(40);
        panelCrud.setAlignment(Pos.CENTER);
        panelCrud.setPadding(new Insets(40, 1, 1, 1));
        Button agregar = new Button("Agregar Empleado");
        agregar.setPrefSize(wbtn, hbtn);
        agregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ventanaAgregarEmpleado();
            }
        });
        panelCrud.getChildren().addAll(agregar);
        //panelCrud.setStyle("-fx-background-color: #e6f3ff");
        subMenu.setBottom(panelCrud);
        return subMenu;
    }

    public void ventanaAgregarEmpleado() {
        Stage nuevaVentana = new Stage();
        nuevaVentana.initModality(Modality.APPLICATION_MODAL);
        BorderPane panelVentana = new BorderPane();
        panelVentana.setPadding(new Insets(20, 20, 20, 20));
        panelVentana.setPickOnBounds(true);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(2, 2, 2, 2));
        Text titulo = new Text("Agregar Empleado");
        titulo.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        HBox panelTitulo = new HBox(titulo);
        panelTitulo.setAlignment(Pos.CENTER);
        panelVentana.setTop(panelTitulo);
        Label rut = new Label("Rut: ");
        grid.add(rut, 0, 1);
        TextField rutText = new TextField();
        grid.add(rutText, 1, 1);
        Label nombre = new Label("Nombre:");
        grid.add(nombre, 0, 2);
        TextField nombreText = new TextField();
        grid.add(nombreText, 1, 2);
        Label apPaterno = new Label("Apellido Paterno:");
        grid.add(apPaterno, 0, 3);
        TextField apPaternoText = new TextField();
        grid.add(apPaternoText, 1, 3);
        Label apMaterno = new Label("Apellido Materno:");
        grid.add(apMaterno, 0, 4);
        TextField apMaternoText = new TextField();
        grid.add(apMaternoText, 1, 4);
        Label direccion = new Label("Dirección:");
        grid.add(direccion, 0, 5);
        TextField direccionText = new TextField();
        grid.add(direccionText, 1, 5);
        Label telefono = new Label("Teléfono:");
        grid.add(telefono, 0, 6);
        TextField telefonoText = new TextField();
        grid.add(telefonoText, 1, 6);
        Label cargo = new Label("Cargo:");
        grid.add(cargo, 0, 7);
        TextField cargoText = new TextField();
        grid.add(cargoText, 1, 7);
        Label mail = new Label("Mail:");
        grid.add(mail, 0, 8);
        TextField mailText = new TextField();
        grid.add(mailText, 1, 8);
        HBox panelBotones = new HBox(20);
        panelBotones.setAlignment(Pos.CENTER);
        Button aceptar = new Button("Aceptar");
        aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                ConsultaChofer chofer = new ConsultaChofer(conexion);
                if (chofer.insertarChofer(rutText.getText(), nombreText.getText(), apPaternoText.getText(), apMaternoText.getText(), direccionText.getText(), telefonoText.getText(), cargoText.getText(), mailText.getText())) {
                    if (chofer.insertarEmpresaTieneEmpleado(rutText.getText(), fk_empresa)) {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Informacion");
                        alert.setHeaderText(null);
                        alert.setContentText("Empleado agregados exitosamente");
                        alert.showAndWait();
                        nuevaVentana.close();
                        btnEmpleado.fire();
                    } else {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setHeaderText(null);
                        alert.setContentText("El empleado NO se agrego a la base de datos");
                        alert.showAndWait();
                        nuevaVentana.close();
                    }

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(null);
                    alert.setContentText("El empleado NO se agrego a la base de datos");
                    alert.showAndWait();
                    nuevaVentana.close();
                }

            }
        });
        Button cancelar = new Button("Cancelar");
        cancelar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuevaVentana.close();
            }
        });
        panelBotones.getChildren().addAll(aceptar, cancelar);
        panelVentana.setBottom(panelBotones);
        panelVentana.setCenter(grid);
        Scene scene = new Scene(panelVentana, 400, 500);
        nuevaVentana.setScene(scene);
        nuevaVentana.show();
    }

    public void ventanaModificarEmpleado(String id) {
        Stage nuevaVentana = new Stage();
        nuevaVentana.initModality(Modality.APPLICATION_MODAL);
        BorderPane panelVentana = new BorderPane();
        panelVentana.setPadding(new Insets(20, 20, 20, 20));
        panelVentana.setPickOnBounds(true);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(2, 2, 2, 2));
        Text titulo = new Text("Modificar Empleado");
        titulo.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        HBox panelTitulo = new HBox(titulo);
        panelTitulo.setAlignment(Pos.CENTER);
        panelVentana.setTop(panelTitulo);

        Label rut = new Label("Rut: ");
        grid.add(rut, 0, 1);
        TextField rutText = new TextField(id);
        rutText.setEditable(false);
        grid.add(rutText, 1, 1);
        Label nombre = new Label("Nombre:");
        grid.add(nombre, 0, 2);
        TextField nombreText = new TextField();
        grid.add(nombreText, 1, 2);
        Label apPaterno = new Label("Apellido Paterno:");
        grid.add(apPaterno, 0, 3);
        TextField apPaternoText = new TextField();
        grid.add(apPaternoText, 1, 3);
        Label apMaterno = new Label("Apellido Materno:");
        grid.add(apMaterno, 0, 4);
        TextField apMaternoText = new TextField();
        grid.add(apMaternoText, 1, 4);
        Label direccion = new Label("Dirección:");
        grid.add(direccion, 0, 5);
        TextField direccionText = new TextField();
        grid.add(direccionText, 1, 5);
        Label telefono = new Label("Teléfono:");
        grid.add(telefono, 0, 6);
        TextField telefonoText = new TextField();
        grid.add(telefonoText, 1, 6);
        Label cargo = new Label("Cargo:");
        grid.add(cargo, 0, 7);
        TextField cargoText = new TextField();
        grid.add(cargoText, 1, 7);
        Label mail = new Label("Mail:");
        grid.add(mail, 0, 8);
        TextField mailText = new TextField();
        grid.add(mailText, 1, 8);
        HBox panelBotones = new HBox(20);
        panelBotones.setAlignment(Pos.CENTER);
        ConsultaChofer chofer = new ConsultaChofer(conexion);
        chofer.mostrarChofer(fk_empresa);
        for (Conductor conductor : chofer.getConductores()) {
            if (conductor.getRutEmpleado().equals(id)) {
                nombreText.setText(conductor.getNombre());
                apPaternoText.setText(conductor.getApellidoPaterno());
                apMaternoText.setText(conductor.getApellidoMaterno());
                direccionText.setText(conductor.getDireccion());
                telefonoText.setText(conductor.getTelefono());
                cargoText.setText(conductor.getLicencias());
                mailText.setText(conductor.getExperiencia());
            }
        }
        Button aceptar = new Button("Aceptar");
        aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                ConsultaChofer chofer = new ConsultaChofer(conexion);
                if (chofer.modificarChofer(id, nombreText.getText(), apPaternoText.getText(), apMaternoText.getText(), direccionText.getText(), telefonoText.getText(), cargoText.getText(), mailText.getText())) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Informacion");
                    alert.setHeaderText(null);
                    alert.setContentText("Empleado agregados exitosamente");
                    alert.showAndWait();
                    nuevaVentana.close();
                    btnEmpleado.fire();

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(null);
                    alert.setContentText("El empleado NO se agrego a la base de datos");
                    alert.showAndWait();
                    nuevaVentana.close();
                }

            }
        });
        Button cancelar = new Button("Cancelar");
        cancelar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuevaVentana.close();
            }
        });
        panelBotones.getChildren().addAll(aceptar, cancelar);
        panelVentana.setBottom(panelBotones);
        panelVentana.setCenter(grid);
        Scene scene = new Scene(panelVentana, 400, 500);
        nuevaVentana.setScene(scene);
        nuevaVentana.show();
    }

    public void ventanaAgregarBus() {
        Stage nuevaVentana = new Stage();
        nuevaVentana.initModality(Modality.APPLICATION_MODAL);
        BorderPane panelVentana = new BorderPane();
        panelVentana.setPadding(new Insets(20, 20, 20, 20));
        panelVentana.setPickOnBounds(true);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(2, 2, 2, 2));
        Text titulo = new Text("Agregar Bus");
        titulo.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        HBox panelTitulo = new HBox(titulo);
        panelTitulo.setAlignment(Pos.CENTER);
        panelVentana.setTop(panelTitulo);

        Label userName = new Label("Patente: ");
        grid.add(userName, 0, 1);
        TextField patente = new TextField();
        grid.add(patente, 1, 1);
        Label marca = new Label("Marca:");
        grid.add(marca, 0, 2);
        TextField marcaText = new TextField();
        grid.add(marcaText, 1, 2);
        Label modelo = new Label("Modelo:");
        grid.add(modelo, 0, 3);
        TextField modeloText = new TextField();
        grid.add(modeloText, 1, 3);
        Label anio = new Label("Año:");
        grid.add(anio, 0, 4);
        TextField anioInt = new TextField();
        grid.add(anioInt, 1, 4);
        Label capacidad = new Label("Capacidad:");
        grid.add(capacidad, 0, 5);
        TextField capacidadText = new TextField();
        grid.add(capacidadText, 1, 5);

        Label empresa = new Label("Conductor:");
        grid.add(empresa, 0, 6);
        ComboBox<String> conductor = new ComboBox<>();
        conductor.setMaxWidth(150);
        grid.add(conductor, 1, 6);

        ConsultaChofer chofer = new ConsultaChofer(conexion);
        chofer.mostrarChofer(fk_empresa);
        for (Conductor chof : chofer.getConductores()) {
            conductor.getItems().add(chof.getNombre());
        }

        HBox panelBotones = new HBox(20);
        panelBotones.setAlignment(Pos.CENTER);
        Button aceptar = new Button("Aceptar");

        aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConsultaBus consultaBus = new ConsultaBus(conexion);
                String rutChofer = "";
                for (Conductor chof : chofer.getConductores()) {
                    if (conductor.getSelectionModel().getSelectedItem().equals(chof.getNombre())) {
                        rutChofer = chof.getRutEmpleado();
                    }
                }
                System.out.println("rut empleado: " + rutChofer);
                if (consultaBus.insertarBus(patente.getText(), marcaText.getText(), modeloText.getText(), Integer.parseInt(anioInt.getText()), Integer.parseInt(capacidadText.getText()), rutChofer)) {
                    if (consultaBus.insertarEmpresaTieneBus(patente.getText(), fk_empresa)) {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Informacion");
                        alert.setHeaderText(null);
                        alert.setContentText("Datos agregados exitosamente");
                        alert.showAndWait();
                        nuevaVentana.close();
                        btnBus.fire();
                    }

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(null);
                    alert.setContentText("Datos NO se agregaron a la base de datos");
                    alert.showAndWait();
                    nuevaVentana.close();
                }

            }
        });
        Button cancelar = new Button("Cancelar");
        cancelar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuevaVentana.close();
            }
        });
        panelBotones.getChildren().addAll(aceptar, cancelar);
        panelVentana.setBottom(panelBotones);
        panelVentana.setCenter(grid);
        Scene scene = new Scene(panelVentana, 400, 400);
        nuevaVentana.setScene(scene);
        nuevaVentana.show();
    }

    public void ventanaModificarBus(String id) {
        Stage nuevaVentana = new Stage();
        nuevaVentana.initModality(Modality.APPLICATION_MODAL);
        BorderPane panelVentana = new BorderPane();
        panelVentana.setPadding(new Insets(20, 20, 20, 20));
        panelVentana.setPickOnBounds(true);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(2, 2, 2, 2));
        Text titulo = new Text("Modificar Bus");
        titulo.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        HBox panelTitulo = new HBox(titulo);
        panelTitulo.setAlignment(Pos.CENTER);
        panelVentana.setTop(panelTitulo);

        Label userName = new Label("Patente: ");
        grid.add(userName, 0, 1);
        TextField patente = new TextField();
        grid.add(patente, 1, 1);
        patente.setEditable(false);
        Label marca = new Label("Marca:");
        grid.add(marca, 0, 2);
        TextField marcaText = new TextField();
        grid.add(marcaText, 1, 2);
        Label modelo = new Label("Modelo:");
        grid.add(modelo, 0, 3);
        TextField modeloText = new TextField();
        grid.add(modeloText, 1, 3);
        Label anio = new Label("Año:");
        grid.add(anio, 0, 4);
        TextField anioInt = new TextField();
        grid.add(anioInt, 1, 4);
        Label capacidad = new Label("Capacidad:");
        grid.add(capacidad, 0, 5);
        TextField capacidadText = new TextField();
        grid.add(capacidadText, 1, 5);

        Label empresa = new Label("Conductor:");
        grid.add(empresa, 0, 6);
        ComboBox<String> conductor = new ComboBox<>();
        conductor.setMaxWidth(150);
        grid.add(conductor, 1, 6);
        ConsultaChofer chofer = new ConsultaChofer(conexion);
        chofer.mostrarChofer(fk_empresa);
        for (Conductor chof : chofer.getConductores()) {
            conductor.getItems().add(chof.getNombre());
        }
        ConsultaBus consultaBus = new ConsultaBus(conexion);
        consultaBus.mostrarBus(fk_empresa);
        for (Bus bus : consultaBus.getBuses()) {
            if (bus.getPatente().equals(id)) {
                patente.setText(bus.getPatente());
                marcaText.setText(bus.getMarca());
                modeloText.setText(bus.getModelo());
                anioInt.setText(Integer.toString(bus.getAnio()));
                capacidadText.setText(Integer.toString(bus.getCapacidad()));
                conductor.setValue(bus.getConductor());
            }
        }

        HBox panelBotones = new HBox(20);
        panelBotones.setAlignment(Pos.CENTER);
        Button aceptar = new Button("Aceptar");
        aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConsultaBus consultaBus = new ConsultaBus(conexion);
                String rutChofer = "";
                for (Conductor chof : chofer.getConductores()) {
                    if (conductor.getSelectionModel().getSelectedItem().equals(chof.getNombre()) || conductor.getSelectionModel().getSelectedItem().equals(chof.getRutEmpleado())) {
                        rutChofer = chof.getRutEmpleado();
                    }
                }
                if (consultaBus.modificarBus(id, marcaText.getText(), modeloText.getText(), Integer.parseInt(anioInt.getText()), Integer.parseInt(capacidadText.getText()), rutChofer)) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Informacion");
                    alert.setHeaderText(null);
                    alert.setContentText("Datos modificados exitosamente");
                    alert.showAndWait();
                    nuevaVentana.close();
                    btnBus.fire();

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(null);
                    alert.setContentText("Datos NO se agregaron a la base de datos");
                    alert.showAndWait();
                    nuevaVentana.close();
                }
            }
        });
        Button cancelar = new Button("Cancelar");
        cancelar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuevaVentana.close();
            }
        });
        panelBotones.getChildren().addAll(aceptar, cancelar);
        panelVentana.setBottom(panelBotones);
        panelVentana.setCenter(grid);
        Scene scene = new Scene(panelVentana, 400, 400);
        nuevaVentana.setScene(scene);
        nuevaVentana.show();
    }

    public void ventanaAgregarSalida() {
        Stage nuevaVentana = new Stage();
        nuevaVentana.initModality(Modality.APPLICATION_MODAL);
        BorderPane panelVentana = new BorderPane();
        panelVentana.setPadding(new Insets(20, 20, 20, 20));
        panelVentana.setPickOnBounds(true);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(2, 2, 2, 2));
        Text titulo = new Text("Agregar Salida");
        titulo.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        HBox panelTitulo = new HBox(titulo);
        panelTitulo.setAlignment(Pos.CENTER);
        panelVentana.setTop(panelTitulo);
        Label rut = new Label("Hora salida: ");
        grid.add(rut, 0, 1);
        TextField rutText = new TextField();
        grid.add(rutText, 1, 1);
        Label nombre = new Label("Hora entrada: ");
        grid.add(nombre, 0, 2);
        TextField nombreText = new TextField();
        grid.add(nombreText, 1, 2);

        Label apPaterno = new Label("Bus: ");
        grid.add(apPaterno, 0, 3);
        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.setMaxWidth(150);
        grid.add(comboBox, 1, 3);

        ConsultaBus consultaBus = new ConsultaBus(conexion);
        consultaBus.mostrarBus(fk_empresa);
        for (Bus bus : consultaBus.getBuses()) {
            comboBox.getItems().add(bus.getPatente());
        }
        
        Label lruta = new Label("Ruta: ");
        grid.add(lruta, 0, 4);
        ComboBox<String> crutas = new ComboBox<>();
        crutas.setMaxWidth(150);
        grid.add(crutas, 1, 4);

        ConsultaRuta consultaRuta = new ConsultaRuta(conexion);
        consultaRuta.mostrarRuta(fk_empresa);
        for (Ruta rutas : consultaRuta.getRutas()) {
            crutas.getItems().add(rutas.getOrigen()+"-"+rutas.getDestino());
        }
        
        HBox panelBotones = new HBox(20);
        panelBotones.setAlignment(Pos.CENTER);
        Button aceptar = new Button("Aceptar");
        aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConsultaSalidaAux consultaSalidaAux = new ConsultaSalidaAux(conexion);
                if (consultaSalidaAux.agregarSalida(rutText.getText(), nombreText.getText(), comboBox.getSelectionModel().getSelectedItem())) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Informacion");
                    alert.setHeaderText(null);
                    alert.setContentText("Salida agregada exitosamente");
                    alert.showAndWait();
                    nuevaVentana.close();
                    btnSalida.fire();

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(null);
                    alert.setContentText("La salida NO se agrego a la base de datos");
                    alert.showAndWait();
                    nuevaVentana.close();
                }

            }
        }
        );
        Button cancelar = new Button("Cancelar");
        cancelar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuevaVentana.close();
            }
        });
        panelBotones.getChildren().addAll(aceptar, cancelar);
        panelVentana.setBottom(panelBotones);
        panelVentana.setCenter(grid);
        Scene scene = new Scene(panelVentana, 400, 400);
        nuevaVentana.setScene(scene);
        nuevaVentana.show();
    }
    
    public void ventanaModificarSalida(String id) {
        Stage nuevaVentana = new Stage();
        nuevaVentana.initModality(Modality.APPLICATION_MODAL);
        BorderPane panelVentana = new BorderPane();
        panelVentana.setPadding(new Insets(20, 20, 20, 20));
        panelVentana.setPickOnBounds(true);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(2, 2, 2, 2));
        Text titulo = new Text("Modificar Salida");
        titulo.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        HBox panelTitulo = new HBox(titulo);
        panelTitulo.setAlignment(Pos.CENTER);
        panelVentana.setTop(panelTitulo);
        Label rut = new Label("Hora salida: ");
        grid.add(rut, 0, 1);
        TextField rutText = new TextField();
        grid.add(rutText, 1, 1);
        Label nombre = new Label("Hora entrada: ");
        grid.add(nombre, 0, 2);
        TextField nombreText = new TextField();
        grid.add(nombreText, 1, 2);

        Label apPaterno = new Label("Bus: ");
        grid.add(apPaterno, 0, 3);
        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.setMaxWidth(150);
        grid.add(comboBox, 1, 3);

        ConsultaBus consultaBus = new ConsultaBus(conexion);
        consultaBus.mostrarBus(fk_empresa);
        for (Bus bus : consultaBus.getBuses()) {
            comboBox.getItems().add(bus.getPatente());
        }
        
        ConsultaSalidaAux consultaSalida = new ConsultaSalidaAux(conexion);
        consultaSalida.mostrarSalida(fk_empresa);
        for (SalidaAux salida : consultaSalida.getSalidas()) {
            System.out.println("aaaaaaaaaaaaaaaaaa id: "+id);
            if (salida.getId().equals(id)) {
                rutText.setText(salida.getFechaSalida());
                nombreText.setText(salida.getFechaSalida());
                comboBox.setValue(salida.getPatente());
            }
        }
        
        HBox panelBotones = new HBox(20);
        panelBotones.setAlignment(Pos.CENTER);
        Button aceptar = new Button("Aceptar");
        aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConsultaSalidaAux consultaSalidaAux = new ConsultaSalidaAux(conexion);
                if (consultaSalidaAux.modificarSalida(id,rutText.getText(), nombreText.getText(), comboBox.getSelectionModel().getSelectedItem())) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Informacion");
                    alert.setHeaderText(null);
                    alert.setContentText("Salida agregada exitosamente");
                    alert.showAndWait();
                    nuevaVentana.close();
                    btnSalida.fire();

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(null);
                    alert.setContentText("La salida NO se agrego a la base de datos");
                    alert.showAndWait();
                    nuevaVentana.close();
                }

            }
        }
        );
        Button cancelar = new Button("Cancelar");
        cancelar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuevaVentana.close();
            }
        });
        panelBotones.getChildren().addAll(aceptar, cancelar);
        panelVentana.setBottom(panelBotones);
        panelVentana.setCenter(grid);
        Scene scene = new Scene(panelVentana, 400, 400);
        nuevaVentana.setScene(scene);
        nuevaVentana.show();
    }

    BorderPane menuBus() {
        PanelUsuario panelUsuario = new PanelUsuario("administrador", usuario);
        HBox panelUsuarioLogeado = new HBox();
        panelUsuarioLogeado.setAlignment(Pos.CENTER_RIGHT);
        panelUsuarioLogeado.getChildren().add(panelUsuario);
        Hyperlink linkSalir = panelUsuario.linkSalir();
        linkSalir.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                scene.setRoot(root);
                primaryStage.setWidth(500);
                primaryStage.setHeight(500);
                primaryStage.setScene(scene);
            }
        });
        titulo = new Text("Administracion de Buses");
        titulo.setFont(Font.font("Arial", FontWeight.BOLD, 18));
        VBox panelTitulo = new VBox(50);
        VBox panelTitul2 = new VBox();
        panelTitulo.getChildren().addAll(panelUsuarioLogeado, titulo, panelTitul2);
        panelTitulo.setAlignment(Pos.CENTER);
        subMenu.setTop(panelTitulo);
        this.setCenter(subMenu);
        //consulta a la base de datos para llenar la tabla
        ConsultaBus bus = new ConsultaBus(getConexion());
        bus.mostrarBus(this.fk_empresa);
        updateTableBus(tablaBus(bus.getBuses()));
        //-----------------------------
        subMenu.setCenter(table);
        this.setCenter(subMenu);
        //agregando crud
        HBox panelCrud = new HBox(40);
        panelCrud.setAlignment(Pos.CENTER);
        panelCrud.setPadding(new Insets(40, 1, 1, 1));
        Button agregar = new Button("Agregar Bus");
        agregar.setPrefSize(wbtn, hbtn);
        agregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ventanaAgregarBus();
            }
        });
        panelCrud.getChildren().addAll(agregar);
        //panelCrud.setStyle("-fx-background-color: #e6f3ff");
        subMenu.setBottom(panelCrud);
        return subMenu;
    }

    BorderPane menuSalida() {
        PanelUsuario panelUsuario = new PanelUsuario("administrador", usuario);
        HBox panelUsuarioLogeado = new HBox();
        panelUsuarioLogeado.setAlignment(Pos.CENTER_RIGHT);
        panelUsuarioLogeado.getChildren().add(panelUsuario);
        Hyperlink linkSalir = panelUsuario.linkSalir();
        linkSalir.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                scene.setRoot(root);
                primaryStage.setWidth(500);
                primaryStage.setHeight(500);
                primaryStage.setScene(scene);
            }
        });
        titulo = new Text("Administracion de Salidas");
        titulo.setFont(Font.font("Arial", FontWeight.BOLD, 18));
        VBox panelTitulo = new VBox(50);
        VBox panelTitul2 = new VBox();
        panelTitulo.getChildren().addAll(panelUsuarioLogeado, titulo, panelTitul2);
        panelTitulo.setAlignment(Pos.CENTER);
        subMenu.setTop(panelTitulo);
        //consulta a la base de datos para llenar la tabla
        ConsultaSalidaAux salida = new ConsultaSalidaAux(getConexion());
        salida.mostrarSalida(this.fk_empresa);
        updateTableSalida(tablaSalida(salida.getSalidas()));
        //-----------------------------
        subMenu.setCenter(table);
        this.setCenter(subMenu);
        //agregando crud
        HBox panelCrud = new HBox(40);
        panelCrud.setAlignment(Pos.CENTER);
        panelCrud.setPadding(new Insets(40, 1, 1, 1));
        Button agregar = new Button("Agregar Salida");
        agregar.setPrefSize(wbtn, hbtn);
        agregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ventanaAgregarSalida();
            }
        });
        panelCrud.getChildren().addAll(agregar);
        //panelCrud.setStyle("-fx-background-color: #e6f3ff");
        subMenu.setBottom(panelCrud);
        return subMenu;
    }

    public String[][] tablaEmpleado(ArrayList<Conductor> conductores) {
        String[][] conduc = new String[conductores.size()][8];
        int i = 0;
        int j = 0;
        for (Conductor conductor : conductores) {
            conduc[i][j] = conductor.getRutEmpleado();
            ++j;
            conduc[i][j] = conductor.getNombre();
            j++;
            conduc[i][j] = conductor.getApellidoPaterno();
            j++;
            conduc[i][j] = conductor.getApellidoMaterno();
            j++;
            conduc[i][j] = conductor.getDireccion();
            j++;
            conduc[i][j] = conductor.getTelefono();
            j++;
            conduc[i][j] = conductor.getLicencias();
            j++;
            conduc[i][j] = conductor.getExperiencia();
            j = 0;
            ++i;
        }
        return conduc;
    }

    public String[][] tablaBus(ArrayList<Bus> buses) {
        String[][] conduc = new String[buses.size()][5];
        int i = 0;
        int j = 0;
        for (Bus bus : buses) {
            conduc[i][j] = bus.getPatente();
            ++j;
            conduc[i][j] = bus.getMarca();
            j++;
            conduc[i][j] = bus.getModelo();
            j++;
            conduc[i][j] = Integer.toString(bus.getAnio());
            j++;
            conduc[i][j] = Integer.toString(bus.getCapacidad());
            j = 0;
            ++i;
        }
        return conduc;
    }

    public String[][] tablaSalida(ArrayList<SalidaAux> salidas) {
        String[][] matrizSalida = new String[salidas.size()][4];
        int i = 0;
        int j = 0;
        for (SalidaAux salida : salidas) {
            matrizSalida[i][j] = String.valueOf(salida.getId());
            ++j;
            matrizSalida[i][j] = String.valueOf(salida.getFechaSalida());
            ++j;
            matrizSalida[i][j] = String.valueOf(salida.getFechaEntrada());
            j++;
            matrizSalida[i][j] = String.valueOf(salida.getPatente());
            j = 0;
            ++i;
        }
        return matrizSalida;
    }

    private void recreateTableEmpleado() {
        this.table = new TableView(FXCollections.observableList(this.data));
        this.table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        this.table.editableProperty().set(false);

        String[] header = {"Rut", "Nombre", "Ap Paterno", "ap Materno", "Direccion", "Telefono", "Cargo", "mail"};
        for (int i = 0; i < 8; i++) {
            TableColumn tc = new TableColumn(header[i]);
            final int col = i;
            if (i == 0) {
                tc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], String>, ObservableValue<String>>() {
                    @Override
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<String[], String> p) {
                        return new SimpleStringProperty(((String[]) p.getValue())[col]);
                    }
                });
            } else {
                tc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], String>, ObservableValue<String>>() {
                    @Override
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<String[], String> p) {
                        if (((String[]) p.getValue())[col].length() <= 0) {
                            return new SimpleStringProperty(((String[]) p.getValue())[col]);
                        }
                        return new SimpleStringProperty(((String[]) p.getValue())[col]);
                    }
                });
            }
            tc.setMinWidth(100.0D);
            this.table.getColumns().add(tc);
        }
        //Insert Button
        TableColumn columnaModificar = new TableColumn<>("Modificar");
        columnaModificar.setMinWidth(80.0D);
        columnaModificar.setStyle("-fx-alignment:CENTER;");
        columnaModificar.setSortable(false);

        columnaModificar.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<String[], Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        columnaModificar.setCellFactory(new Callback<TableColumn<String[], Boolean>, TableCell<String[], Boolean>>() {

            @Override
            public TableCell<String[], Boolean> call(TableColumn<String[], Boolean> param) {
                return new botonModificarEmpleado();
            }
        });
        this.table.getColumns().add(columnaModificar);

        //Insert Button
        TableColumn columnaEliminar = new TableColumn<>("Eliminar");
        columnaEliminar.setMinWidth(80.0D);
        columnaEliminar.setStyle("-fx-alignment:CENTER;");
        columnaEliminar.setSortable(false);

        columnaEliminar.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<String[], Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        columnaEliminar.setCellFactory(new Callback<TableColumn<String[], Boolean>, TableCell<String[], Boolean>>() {

            @Override
            public TableCell<String[], Boolean> call(TableColumn<String[], Boolean> param) {
                return new botonEliminarEmpleado();
            }
        });
        this.table.getColumns().add(columnaEliminar);
    }

    private void recreateTableBus() {
        this.table = new TableView(FXCollections.observableList(this.data));
        this.table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        this.table.editableProperty().set(false);

        String[] header = {"Patente", "Marca", "Modelo", "Año", "Capacidad"};
        for (int i = 0; i < 5; i++) {
            TableColumn tc = new TableColumn(header[i]);
            final int col = i;
            if (i == 0) {
                tc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], String>, ObservableValue<String>>() {
                    @Override
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<String[], String> p) {
                        return new SimpleStringProperty(((String[]) p.getValue())[col]);
                    }
                });
            } else {
                tc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], String>, ObservableValue<String>>() {
                    @Override
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<String[], String> p) {
                        if (((String[]) p.getValue())[col].length() <= 0) {
                            return new SimpleStringProperty(((String[]) p.getValue())[col]);
                        }
                        return new SimpleStringProperty(((String[]) p.getValue())[col]);
                    }
                });
            }
            tc.setMinWidth(100.0D);
            this.table.getColumns().add(tc);
        }
        //Insert Button
        TableColumn columnaModificar = new TableColumn<>("Modificar");
        columnaModificar.setMinWidth(80.0D);
        columnaModificar.setStyle("-fx-alignment:CENTER;");
        columnaModificar.setSortable(false);

        columnaModificar.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<String[], Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        columnaModificar.setCellFactory(new Callback<TableColumn<String[], Boolean>, TableCell<String[], Boolean>>() {

            @Override
            public TableCell<String[], Boolean> call(TableColumn<String[], Boolean> param) {
                return new botonModificarBus();
            }
        });
        this.table.getColumns().add(columnaModificar);

        //Insert Button
        TableColumn columnaEliminar = new TableColumn<>("Eliminar");
        columnaEliminar.setMinWidth(80.0D);
        columnaEliminar.setStyle("-fx-alignment:CENTER;");
        columnaEliminar.setSortable(false);

        columnaEliminar.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<String[], Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        columnaEliminar.setCellFactory(new Callback<TableColumn<String[], Boolean>, TableCell<String[], Boolean>>() {

            @Override
            public TableCell<String[], Boolean> call(TableColumn<String[], Boolean> param) {
                return new botonEliminarBus();
            }
        });
        this.table.getColumns().add(columnaEliminar);
    }

    //Define the button cell
    private void recreateTableSalida() {
        this.table = new TableView(FXCollections.observableList(this.data));
        this.table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        this.table.editableProperty().set(false);

        String[] header = {"id","Fecha Salida", "Fecha Entrada", "Bus"};
        for (int i = 0; i < 4; i++) {
            TableColumn tc = new TableColumn(header[i]);
            final int col = i;
            if (i == 0) {
                tc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], String>, ObservableValue<String>>() {
                    @Override
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<String[], String> p) {
                        return new SimpleStringProperty(((String[]) p.getValue())[col]);
                    }
                });
            } else {
                tc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], String>, ObservableValue<String>>() {
                    @Override
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<String[], String> p) {
                        if (((String[]) p.getValue())[col].length() <= 0) {
                            return new SimpleStringProperty(((String[]) p.getValue())[col]);
                        }
                        return new SimpleStringProperty(((String[]) p.getValue())[col]);
                    }
                });
            }
            tc.setMinWidth(100.0D);
            this.table.getColumns().add(tc);
        }
        //Insert Button
        TableColumn columnaModificar = new TableColumn<>("Modificar");
        columnaModificar.setMinWidth(80.0D);
        columnaModificar.setStyle("-fx-alignment:CENTER;");
        columnaModificar.setSortable(false);

        columnaModificar.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<String[], Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        columnaModificar.setCellFactory(new Callback<TableColumn<String[], Boolean>, TableCell<String[], Boolean>>() {

            @Override
            public TableCell<String[], Boolean> call(TableColumn<String[], Boolean> param) {
                return new botonModificarSalida();
            }
        });
        this.table.getColumns().add(columnaModificar);

        //Insert Button
        TableColumn columnaEliminar = new TableColumn<>("Eliminar");
        columnaEliminar.setMinWidth(80.0D);
        columnaEliminar.setStyle("-fx-alignment:CENTER;");
        columnaEliminar.setSortable(false);

        columnaEliminar.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<String[], Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        columnaEliminar.setCellFactory(new Callback<TableColumn<String[], Boolean>, TableCell<String[], Boolean>>() {

            @Override
            public TableCell<String[], Boolean> call(TableColumn<String[], Boolean> param) {
                return new botonEliminarSalida();
            }
        });
        this.table.getColumns().add(columnaEliminar);
    }

    public boolean updateTableEmpleado(String[][] max) {
        data = FXCollections.observableArrayList();
        data.addAll(Arrays.asList(max));
        recreateTableEmpleado();
        return true;
    }

    public boolean updateTableBus(String[][] max) {
        data = FXCollections.observableArrayList();
        data.addAll(Arrays.asList(max));
        recreateTableBus();
        return true;
    }

    public boolean updateTableSalida(String[][] max) {
        data = FXCollections.observableArrayList();
        data.addAll(Arrays.asList(max));
        recreateTableSalida();
        return true;
    }

    public Connection getConexion() {
        return conexion;
    }

    public double getW() {
        return w;
    }

    public double getH() {
        return h;
    }

    public void ventanaEmergente() {

    }

    public void ventantaModificarParadero(int idRuta) {
        Stage nuevaVentana = new Stage();
        nuevaVentana.initModality(Modality.APPLICATION_MODAL);
        BorderPane panelVentana = new BorderPane();
        panelVentana.setPadding(new Insets(20, 20, 20, 20));
        panelVentana.setPickOnBounds(true);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(2, 2, 2, 2));
        Text titulo = new Text("Agregar Paradero");
        titulo.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        HBox panelTitulo = new HBox(titulo);
        panelTitulo.setAlignment(Pos.CENTER);
        panelVentana.setTop(panelTitulo);

        Label userName = new Label("Nombre Paradero: ");
        grid.add(userName, 0, 1);
        TextField nombre = new TextField();
        grid.add(nombre, 1, 1);
        Label pw = new Label("Ubicación Paradero:");
        grid.add(pw, 0, 2);
        TextField ubicacion = new TextField();
        grid.add(ubicacion, 1, 2);
        HBox panelBotones = new HBox(20);
        panelBotones.setAlignment(Pos.CENTER);
        Button aceptar = new Button("Aceptar");
        aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConsultaParadero consultaParadero = new ConsultaParadero(conexion);
                ConsultaRutaTieneParadero rutaTieneParadero = new ConsultaRutaTieneParadero(conexion);
                if (consultaParadero.insertarParadero(nombre.getText(), ubicacion.getText())) {

                    if (rutaTieneParadero.insertarRutaTieneParadero(idRuta, consultaParadero.getUltimoParadero())) {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Información");
                        alert.setHeaderText(null);
                        alert.setContentText("Datos agregados exitosamente");
                        alert.showAndWait();
                        nuevaVentana.close();
                    }
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(null);
                    alert.setContentText("Datos NO se agregaron a la base de datos");
                    alert.showAndWait();
                    nuevaVentana.close();
                }

            }
        });
        Button cancelar = new Button("Cancelar");
        cancelar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuevaVentana.close();
            }
        });
        panelBotones.getChildren().addAll(aceptar, cancelar);
        panelVentana.setBottom(panelBotones);
        panelVentana.setCenter(grid);
        Scene scene = new Scene(panelVentana, 400, 400);
        nuevaVentana.setScene(scene);
        nuevaVentana.show();

    }

    private class botonModificarEmpleado extends TableCell<String[], Boolean> {

        final Button cellButton = new Button("Modificar");

        botonModificarEmpleado() {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    int selectdIndex = getTableRow().getIndex();
                    String[] tc = table.getItems().get(selectdIndex);
                    int indiceClave = 0;
                    String id = tc[indiceClave];
                    System.out.println("id ruta: " + id);
                    ventanaModificarEmpleado(id);
                    //obtengo el id como referencia
                    //ventantaModificarRuta(Integer.parseInt(id));
                }
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

    private class botonEliminarEmpleado extends TableCell<String[], Boolean> {

        final Button cellButton = new Button("Eliminar");

        botonEliminarEmpleado() {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    int selectdIndex = getTableRow().getIndex();
                    String[] tc = table.getItems().get(selectdIndex);
                    int indiceClave = 0;
                    String id = tc[indiceClave];
                    System.out.println("id ruta: " + id);
                    //obtengo el id como referencia
                    ConsultaChofer chofer = new ConsultaChofer(getConexion());
                    chofer.eliminarEmpresaTieneEmpleado(id);
                    chofer.eliminarChofer(id);
                    btnEmpleado.fire();
                }
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

    private class botonModificarBus extends TableCell<String[], Boolean> {

        final Button cellButton = new Button("Modificar");

        botonModificarBus() {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    int selectdIndex = getTableRow().getIndex();
                    String[] tc = table.getItems().get(selectdIndex);
                    int indiceClave = 0;
                    String id = tc[indiceClave];
                    System.out.println("id ruta: " + id);
                    ventanaModificarBus(id);
                }
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

    private class botonEliminarBus extends TableCell<String[], Boolean> {

        final Button cellButton = new Button("Eliminar");

        botonEliminarBus() {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    int selectdIndex = getTableRow().getIndex();
                    String[] tc = table.getItems().get(selectdIndex);
                    int indiceClave = 0;
                    String id = tc[indiceClave];
                    System.out.println("id ruta: " + id);
                    ConsultaBus bus = new ConsultaBus(getConexion());
                    bus.eliminarEmpresaTieneBus(id);
                    bus.eliminarBus(id);
                    btnBus.fire();
                    //obtengo el id como referencia
                    //ventantaModificarRuta(Integer.parseInt(id));
                }
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }
    private class botonModificarSalida extends TableCell<String[], Boolean> {

        final Button cellButton = new Button("Modificar");

        botonModificarSalida() {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    int selectdIndex = getTableRow().getIndex();
                    String[] tc = table.getItems().get(selectdIndex);
                    int indiceClave = 0;
                    String id = tc[indiceClave];
                    System.out.println("id ruta: " + id);
                    ventanaModificarSalida(id);
                    //obtengo el id como referencia
                    //ventantaModificarRuta(Integer.parseInt(id));
                }
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }
    private class botonEliminarSalida extends TableCell<String[], Boolean> {

        final Button cellButton = new Button("Eliminar");

        botonEliminarSalida() {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    int selectdIndex = getTableRow().getIndex();
                    String[] tc = table.getItems().get(selectdIndex);
                    int indiceClave = 0;
                    String id = tc[indiceClave];
                    System.out.println("id ruta: " + id);
                    ConsultaSalidaAux consultaSalida = new ConsultaSalidaAux(getConexion());
                    consultaSalida.eliminarSalidaAux(id);
                    btnSalida.fire();
                    //obtengo el id como referencia
                    //ventantaModificarRuta(Integer.parseInt(id));
                }
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

    public void setThis(BorderPane a) {
        this.subMenu = a;
    }
}
