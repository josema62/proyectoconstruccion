/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import ModeloDatos.Paradero;
import ModeloDatos.Ruta;
import conexion.ConsultaEmpresaTieneRuta;
import conexion.ConsultaParadero;
import conexion.ConsultaRuta;
import conexion.ConsultaRutaTieneParadero;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 *
 * @author crist
 */
public class VistaRutas extends VistaAdministrador {

    public VistaRutas(Scene scene, Connection conexion, Stage primaryStage, StackPane root, String usuario, String fk_empresa) {
        super(scene, conexion, primaryStage, root, usuario, fk_empresa);
        this.usuario = usuario;
        this.fk_empresa = fk_empresa;
        menuRuta();
    }

    BorderPane menuRuta() {
        PanelUsuario panelUsuario = new PanelUsuario("administrador", usuario);
        HBox panelUsuarioLogeado = new HBox();
        panelUsuarioLogeado.setAlignment(Pos.CENTER_RIGHT);
        panelUsuarioLogeado.getChildren().add(panelUsuario);
        Hyperlink linkSalir = panelUsuario.linkSalir();
        linkSalir.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                scene.setRoot(root);
                primaryStage.setWidth(500);
                primaryStage.setHeight(500);
                primaryStage.setScene(scene);
            }
        });
        titulo = new Text("Administracion de Rutas");
        titulo.setFont(Font.font("Arial", FontWeight.BOLD, 18));
        VBox panelTitulo = new VBox(50);
        VBox panelTitul2 = new VBox();
        panelTitulo.getChildren().addAll(panelUsuarioLogeado, titulo, panelTitul2);
        panelTitulo.setAlignment(Pos.CENTER);
        subMenu.setTop(panelTitulo);
        //consulta a la base de datos para llenar la tabla
        ConsultaRuta ruta = new ConsultaRuta(getConexion());
        System.out.println("fk empresa: " + fk_empresa);
        ruta.mostrarRuta(fk_empresa);
        updateTableRuta(tablaRuta(ruta.getRutas()));
        //-----------------------------
        subMenu.setCenter(table);
        this.setCenter(subMenu);
        //agregando crud
        HBox panelCrud = new HBox(40);
        panelCrud.setAlignment(Pos.CENTER);
        panelCrud.setPadding(new Insets(40, 1, 1, 1));
        Button agregar = new Button("Agregar Ruta");
        agregar.setPrefSize(wbtn, hbtn);
        agregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ventanaAgregarRuta();
            }
        });

        panelCrud.getChildren().addAll(agregar);
        subMenu.setPadding(new Insets(10, 10, 40, 10));
        subMenu.setStyle("-fx-background-color: #e6e6e6");
        subMenu.setBottom(panelCrud);
        return subMenu;
    }

    public String[][] tablaRuta(ArrayList<Ruta> rutas) {
        String[][] matrizRuta = new String[rutas.size()][4];
        int i = 0;
        int j = 0;
        for (Ruta ruta : rutas) {
            matrizRuta[i][j] = Integer.toString(ruta.getId());
            j++;
            matrizRuta[i][j] = ruta.getOrigen();
            j++;
            matrizRuta[i][j] = ruta.getDestino();
            j++;
            matrizRuta[i][j] = ruta.getDistancia();
            j = 0;
            ++i;
        }
        return matrizRuta;
    }

    private void recreateTableRuta() {
        this.table = new TableView(FXCollections.observableList(this.data));
        this.table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        //this.table.editableProperty().set(true);
        String[] header = {"id","Origen", "Destino", "Distancia"};
        for (int i =0; i < 4; i++) {
            TableColumn tc = new TableColumn(header[i]);
            final int col = i;

            tc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<String[], String> p) {
                    return new SimpleStringProperty(((String[]) p.getValue())[col]);
                }
            });

            tc.setMinWidth(100.0D);
            this.table.getColumns().add(tc);
        }

        //Insert Button
        TableColumn columnaParadero = new TableColumn<>("Paraderos");
        columnaParadero.setMinWidth(80.0D);
        columnaParadero.setStyle("-fx-alignment:CENTER;");
        columnaParadero.setSortable(false);
        columnaParadero.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<String[], Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        columnaParadero.setCellFactory(new Callback<TableColumn<String[], Boolean>, TableCell<String[], Boolean>>() {

            @Override
            public TableCell<String[], Boolean> call(TableColumn<String[], Boolean> param) {
                return new VistaRutas.botonParadero();
            }
        });
        this.table.getColumns().add(columnaParadero);

        //Insert Button
        TableColumn columnaModificar = new TableColumn<>("Modificar");
        columnaModificar.setMinWidth(80.0D);
        columnaModificar.setStyle("-fx-alignment:CENTER;");
        columnaModificar.setSortable(false);

        columnaModificar.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<String[], Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        columnaModificar.setCellFactory(new Callback<TableColumn<String[], Boolean>, TableCell<String[], Boolean>>() {

            @Override
            public TableCell<String[], Boolean> call(TableColumn<String[], Boolean> param) {
                return new VistaRutas.botonModificarRuta();
            }
        });
        this.table.getColumns().add(columnaModificar);

        //Insert Button
        TableColumn columnaEliminar = new TableColumn<>("Eliminar");
        columnaEliminar.setMinWidth(80.0D);
        columnaEliminar.setStyle("-fx-alignment:CENTER;");
        columnaEliminar.setSortable(false);

        columnaEliminar.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<String[], Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        columnaEliminar.setCellFactory(new Callback<TableColumn<String[], Boolean>, TableCell<String[], Boolean>>() {

            @Override
            public TableCell<String[], Boolean> call(TableColumn<String[], Boolean> param) {
                return new VistaRutas.botonEliminarRuta();
            }
        });
        this.table.getColumns().add(columnaEliminar);

    }

    public boolean updateTableRuta(String[][] max) {
        data = FXCollections.observableArrayList();
        data.addAll(Arrays.asList(max));
        recreateTableRuta();
        return true;
    }

    /**
     * Boton table view de tabla ruta
     */
    private class botonParadero extends TableCell<String[], Boolean> {

        final Button cellButton = new Button("Ver paraderos");

        botonParadero() {

            cellButton.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent t) {
                    int selectdIndex = getTableRow().getIndex();
                    String[] tc = table.getItems().get(selectdIndex);
                    int indiceClave = 0;
                    String id = tc[indiceClave];
                    System.out.println("id ruta: " + id);
                    //ConsultaParadero consultaParadero = new ConsultaParadero(conexion);
                    //consultaParadero.consultaParaderosPorRuta(Integer.parseInt(id));
                    menuParaderosDeRuta(Integer.parseInt(id));
                    // Aca con el id se hace la consulta a la base de datos para
                    // obtrener los paraderos referentes a ese id
                }
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            }
        }

    }

    private class botonModificarRuta extends TableCell<String[], Boolean> {

        final Button cellButton = new Button("Modificar");
        botonModificarRuta() {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    int selectdIndex = getTableRow().getIndex();
                    String[] tc = table.getItems().get(selectdIndex);
                    int indiceClave = 0;
                    String id = tc[indiceClave];
                    System.out.println("id ruta: " + id);
                    //obtengo el id como referencia
                    ventantaModificarRuta(Integer.parseInt(id));
                }
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

    private class botonEliminarRuta extends TableCell<String[], Boolean> {

        final Button cellButton = new Button("Eliminar");

        botonEliminarRuta() {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    int selectdIndex = getTableRow().getIndex();
                    String[] tc = table.getItems().get(selectdIndex);
                    int indiceClave = 0;
                    String id = tc[indiceClave];
                    System.out.println("id ruta: " + id);
                    //obtengo el id como referencia
                    ventantaEliminarRuta(Integer.parseInt(id));
                }
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

    /**
     * Boton table view de tabla de paradero
     */
    private class botonModificarParadero extends TableCell<String[], Boolean> {

        final Button cellButton = new Button("Modificar");

        botonModificarParadero() {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    int selectdIndex = getTableRow().getIndex();
                    String[] tc = table.getItems().get(selectdIndex);
                    int indiceClave = 0;
                    String id = tc[indiceClave];
                    System.out.println("id ruta: " + id);
                    //obtengo el id como referencia
                    ventanaModificarParadero(Integer.parseInt(id));
                }
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

    private class botonEliminarParadero extends TableCell<String[], Boolean> {

        final Button cellButton = new Button("Eliminar");

        botonEliminarParadero() {
            cellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    int selectdIndex = getTableRow().getIndex();
                    String[] tc = table.getItems().get(selectdIndex);
                    int indiceClave = 0;
                    String id = tc[indiceClave];
                    System.out.println("id ruta: " + id);
                    //obtengo el id como referencia
                    ventantaEliminarParadero(Integer.parseInt(id));
                }
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

    BorderPane menuParaderosDeRuta(int idRuta) {
        PanelUsuario panelUsuario = new PanelUsuario("administrador", usuario);
        HBox panelUsuarioLogeado = new HBox();
        panelUsuarioLogeado.setAlignment(Pos.CENTER_RIGHT);
        panelUsuarioLogeado.getChildren().add(panelUsuario);
        Hyperlink linkSalir = panelUsuario.linkSalir();
        linkSalir.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                scene.setRoot(root);
                primaryStage.setWidth(500);
                primaryStage.setHeight(500);
                primaryStage.setScene(scene);
            }
        });
        titulo = new Text("Administracion de Paraderos de Ruta: " + idRuta);
        titulo.setFont(Font.font("Arial", FontWeight.BOLD, 18));
        VBox panelTitulo = new VBox(50);
        VBox panelTitul2 = new VBox();
        panelTitulo.getChildren().addAll(panelUsuarioLogeado, titulo, panelTitul2);
        panelTitulo.setAlignment(Pos.CENTER);
        subMenu.setTop(panelTitulo);
        //consulta a la base de datos para llenar la tabla
        ConsultaParadero paradero = new ConsultaParadero(getConexion());
        paradero.consultaParaderosPorRuta(idRuta);
        updateTableParadero(tablaParadero(paradero.getParaderos()));
        //-----------------------------
        subMenu.setCenter(table);
        this.setCenter(subMenu);
        //agregando crud
        HBox panelCrud = new HBox(40);
        panelCrud.setAlignment(Pos.CENTER);
        panelCrud.setPadding(new Insets(40, 1, 1, 1));
        Button agregar = new Button("Agregar Paradero");
        agregar.setPrefSize(wbtn, hbtn);
        agregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ventanaAgregarParadero(idRuta);
            }
        });
        panelCrud.getChildren().addAll(agregar);
        //panelCrud.setStyle("-fx-background-color: #e6f3ff");
        subMenu.setBottom(panelCrud);
        return subMenu;
    }

    public String[][] tablaParadero(ArrayList<Paradero> paraderos) {
        String[][] matrizParadero = new String[paraderos.size()][3];
        int i = 0;
        int j = 0;
        for (Paradero paradero : paraderos) {
            matrizParadero[i][j] = paradero.getId();
            ++j;
            matrizParadero[i][j] = paradero.getNombre();
            j++;
            matrizParadero[i][j] = paradero.getUbicacion();
            j = 0;
            ++i;
        }
        return matrizParadero;
    }

    public boolean updateTableParadero(String[][] max) {
        data = FXCollections.observableArrayList();
        data.addAll(Arrays.asList(max));
        recreateTableParadero();
        return true;
    }

    private void recreateTableParadero() {
        this.table = new TableView(FXCollections.observableList(this.data));
        this.table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        //this.table.editableProperty().set(false);

        String[] header = {"Id", "Nombre", "Ubicacion"};
        for (int i = 0; i < 3; i++) {
            TableColumn tc = new TableColumn(header[i]);
            final int col = i;
            if (i == 0) {
                tc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], String>, ObservableValue<String>>() {
                    @Override
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<String[], String> p) {
                        return new SimpleStringProperty(((String[]) p.getValue())[col]);
                    }
                });
            } else {
                tc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], String>, ObservableValue<String>>() {
                    @Override
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<String[], String> p) {
                        if (((String[]) p.getValue())[col].length() <= 0) {
                            return new SimpleStringProperty(((String[]) p.getValue())[col]);
                        }
                        return new SimpleStringProperty(((String[]) p.getValue())[col]);
                    }
                });
            }
            tc.setMinWidth(100.0D);
            this.table.getColumns().add(tc);
        }
        //Insert Button
        TableColumn columnaModificar = new TableColumn<>("Modificar");
        columnaModificar.setMinWidth(80.0D);
        columnaModificar.setStyle("-fx-alignment:CENTER;");
        columnaModificar.setSortable(false);

        columnaModificar.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<String[], Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        columnaModificar.setCellFactory(new Callback<TableColumn<String[], Boolean>, TableCell<String[], Boolean>>() {

            @Override
            public TableCell<String[], Boolean> call(TableColumn<String[], Boolean> param) {
                return new VistaRutas.botonModificarParadero();
            }
        });
        this.table.getColumns().add(columnaModificar);

        //Insert Button
        TableColumn columnaEliminar = new TableColumn<>("Eliminar");
        columnaEliminar.setMinWidth(80.0D);
        columnaEliminar.setStyle("-fx-alignment:CENTER;");
        columnaEliminar.setSortable(false);

        columnaEliminar.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<String[], Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<String[], Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        columnaEliminar.setCellFactory(new Callback<TableColumn<String[], Boolean>, TableCell<String[], Boolean>>() {

            @Override
            public TableCell<String[], Boolean> call(TableColumn<String[], Boolean> param) {
                return new VistaRutas.botonEliminarParadero();
            }
        });
        this.table.getColumns().add(columnaEliminar);
    }

    /**
     * Crud de Rutas
     */
    public void ventanaAgregarRuta() {
        Stage nuevaVentana = new Stage();
        nuevaVentana.initModality(Modality.APPLICATION_MODAL);
        BorderPane panelVentana = new BorderPane();
        panelVentana.setPadding(new Insets(20, 20, 20, 20));
        panelVentana.setPickOnBounds(true);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(2, 2, 2, 2));
        Text titulo = new Text("Agregar Ruta");
        titulo.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        HBox panelTitulo = new HBox(titulo);
        panelTitulo.setAlignment(Pos.CENTER);
        panelVentana.setTop(panelTitulo);

        Label userName = new Label("Origen Ruta: ");
        grid.add(userName, 0, 1);
        TextField origen = new TextField();
        grid.add(origen, 1, 1);

        Label pw = new Label("Destino Ruta:");
        grid.add(pw, 0, 2);
        TextField destino = new TextField();
        grid.add(destino, 1, 2);

        Label distancia = new Label("Distancia Ruta: ");
        grid.add(distancia, 0, 3);
        TextField tDistancia = new TextField();
        grid.add(tDistancia, 1, 3);

        HBox panelBotones = new HBox(20);
        panelBotones.setAlignment(Pos.CENTER);
        Button aceptar = new Button("Aceptar");
        aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConsultaRuta ruta = new ConsultaRuta(getConexion());
                if (ruta.insertarRuta(origen.getText(), destino.getText(), tDistancia.getText())) {
                    ConsultaEmpresaTieneRuta consultaEmpresaTieneRuta = new ConsultaEmpresaTieneRuta(getConexion());
                    int ultimo = consultaEmpresaTieneRuta.ultimoRutaAgregada();
                    System.out.println("ruta: " + ultimo);
                    if (consultaEmpresaTieneRuta.insertarEmpresaTieneRuta(fk_empresa, ultimo)) {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Información");
                        alert.setHeaderText(null);
                        alert.setContentText("Datos agregados exitosamente");
                        alert.showAndWait();
                        nuevaVentana.close();
                        btnRuta.fire();
                    }

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(null);
                    alert.setContentText("Datos NO se agregaron a la base de datos");
                    alert.showAndWait();
                    nuevaVentana.close();
                }

            }
        });
        Button cancelar = new Button("Cancelar");
        cancelar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuevaVentana.close();
            }
        });
        panelBotones.getChildren().addAll(aceptar, cancelar);
        panelVentana.setBottom(panelBotones);
        panelVentana.setCenter(grid);
        Scene scene = new Scene(panelVentana, 400, 400);
        nuevaVentana.setScene(scene);
        nuevaVentana.show();
    }

    public void ventantaModificarRuta(int idRuta) {
        Stage nuevaVentana = new Stage();
        nuevaVentana.initModality(Modality.APPLICATION_MODAL);
        BorderPane panelVentana = new BorderPane();
        panelVentana.setPadding(new Insets(20, 20, 20, 20));
        panelVentana.setPickOnBounds(true);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(2, 2, 2, 2));
        Text titulo = new Text("Modificar Ruta");
        titulo.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        HBox panelTitulo = new HBox(titulo);
        panelTitulo.setAlignment(Pos.CENTER);
        panelVentana.setTop(panelTitulo);

        Label userName = new Label("Origen Ruta: ");
        grid.add(userName, 0, 1);
        TextField origen = new TextField();

        Label pw = new Label("Destino Ruta:");
        grid.add(pw, 0, 2);
        TextField destino = new TextField();

        Label lDistancia = new Label("Distancia Ruta:");
        grid.add(lDistancia, 0, 3);
        TextField distancia = new TextField();
        ConsultaRuta consultaRuta = new ConsultaRuta(getConexion());
        consultaRuta.mostrarRuta(fk_empresa);
        for (Ruta ruta : consultaRuta.getRutas()) {
            if (ruta.getId() == idRuta) {
                origen.setText(ruta.getOrigen());
                destino.setText(ruta.getDestino());
                distancia.setText(ruta.getDistancia());
            }
        }
        grid.add(origen, 1, 1);
        grid.add(destino, 1, 2);
        grid.add(distancia, 1, 3);
        HBox panelBotones = new HBox(20);
        panelBotones.setAlignment(Pos.CENTER);
        Button aceptar = new Button("Aceptar");
        aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if (consultaRuta.modificarRuta(idRuta, origen.getText(), destino.getText(), distancia.getText())) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Información");
                    alert.setHeaderText(null);
                    alert.setContentText("Datos modificados exitosamente");
                    alert.showAndWait();
                    nuevaVentana.close();
                    btnRuta.fire();

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(null);
                    alert.setContentText("Datos NO se agregaron a la base de datos");
                    alert.showAndWait();
                    nuevaVentana.close();
                }

            }
        });
        Button cancelar = new Button("Cancelar");
        cancelar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuevaVentana.close();
            }
        });
        panelBotones.getChildren().addAll(aceptar, cancelar);
        panelVentana.setBottom(panelBotones);
        panelVentana.setCenter(grid);
        Scene scene = new Scene(panelVentana, 400, 400);
        nuevaVentana.setScene(scene);
        nuevaVentana.show();
    }

    public void ventantaEliminarRuta(int idRuta) {
        ConsultaRuta consultaRuta = new ConsultaRuta(getConexion());
        if (consultaRuta.eliminarRuta(idRuta)) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Información");
            alert.setHeaderText(null);
            alert.setContentText("Datos eliminados exitosamente!");
            alert.showAndWait();

            btnRuta.fire();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("Datos NO se eliminaron");
            alert.showAndWait();
        }

    }

    /**
     * Crud de Paraderos
     */
    public void ventanaAgregarParadero(int idRuta) {
        Stage nuevaVentana = new Stage();
        nuevaVentana.initModality(Modality.APPLICATION_MODAL);
        BorderPane panelVentana = new BorderPane();
        panelVentana.setPadding(new Insets(20, 20, 20, 20));
        panelVentana.setPickOnBounds(true);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(2, 2, 2, 2));
        Text titulo = new Text("Agregar Paradero");
        titulo.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        HBox panelTitulo = new HBox(titulo);
        panelTitulo.setAlignment(Pos.CENTER);
        panelVentana.setTop(panelTitulo);

        Label userName = new Label("Nombre Paradero: ");
        grid.add(userName, 0, 1);
        TextField nombre = new TextField();
        grid.add(nombre, 1, 1);
        Label pw = new Label("Ubicación Paradero:");
        grid.add(pw, 0, 2);
        TextField ubicacion = new TextField();
        grid.add(ubicacion, 1, 2);
        HBox panelBotones = new HBox(20);
        panelBotones.setAlignment(Pos.CENTER);
        Button aceptar = new Button("Aceptar");
        aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConsultaParadero consultaParadero = new ConsultaParadero(getConexion());
                ConsultaRutaTieneParadero rutaTieneParadero = new ConsultaRutaTieneParadero(getConexion());
                if (consultaParadero.insertarParadero(nombre.getText(), ubicacion.getText())) {

                    if (rutaTieneParadero.insertarRutaTieneParadero(idRuta, consultaParadero.getUltimoParadero())) {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Información");
                        alert.setHeaderText(null);
                        alert.setContentText("Datos agregados exitosamente");
                        alert.showAndWait();
                        nuevaVentana.close();
                    }
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(null);
                    alert.setContentText("Datos NO se agregaron a la base de datos");
                    alert.showAndWait();
                    nuevaVentana.close();
                }

            }
        });
        Button cancelar = new Button("Cancelar");
        cancelar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuevaVentana.close();
            }
        });
        panelBotones.getChildren().addAll(aceptar, cancelar);
        panelVentana.setBottom(panelBotones);
        panelVentana.setCenter(grid);
        Scene scene = new Scene(panelVentana, 400, 400);
        nuevaVentana.setScene(scene);
        nuevaVentana.show();
    }

    public void ventanaModificarParadero(int idRuta) {
        Stage nuevaVentana = new Stage();
        nuevaVentana.initModality(Modality.APPLICATION_MODAL);
        BorderPane panelVentana = new BorderPane();
        panelVentana.setPadding(new Insets(20, 20, 20, 20));
        panelVentana.setPickOnBounds(true);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(2, 2, 2, 2));
        Text titulo = new Text("Modificar Paradero");
        titulo.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        HBox panelTitulo = new HBox(titulo);
        panelTitulo.setAlignment(Pos.CENTER);
        panelVentana.setTop(panelTitulo);

        Label userName = new Label("Nombre Paradero: ");
        grid.add(userName, 0, 1);
        TextField nombre = new TextField();
        grid.add(nombre, 1, 1);
        Label pw = new Label("Ubicación Paradero:");
        grid.add(pw, 0, 2);
        TextField ubicacion = new TextField();
        grid.add(ubicacion, 1, 2);
        ConsultaParadero consultaParadero = new ConsultaParadero(getConexion());
        consultaParadero.consultaParaderosPorRuta(idRuta);
        for (Paradero paradero : consultaParadero.getParaderos()) {
            if (Integer.parseInt(paradero.getId()) == idRuta) {
                nombre.setText(paradero.getNombre());
                ubicacion.setText(paradero.getUbicacion());
            }
        }
        HBox panelBotones = new HBox(20);
        panelBotones.setAlignment(Pos.CENTER);
        Button aceptar = new Button("Aceptar");
        aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConsultaParadero consultaParadero = new ConsultaParadero(conexion);
                ConsultaRutaTieneParadero rutaTieneParadero = new ConsultaRutaTieneParadero(conexion);
                if (consultaParadero.modificarParadero(idRuta, nombre.getText(), ubicacion.getText())) {

                    if (rutaTieneParadero.insertarRutaTieneParadero(idRuta, consultaParadero.getUltimoParadero())) {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Información");
                        alert.setHeaderText(null);
                        alert.setContentText("Datos modificados exitosamente");
                        alert.showAndWait();
                        nuevaVentana.close();
                    }
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(null);
                    alert.setContentText("Datos NO se modificaron los datos");
                    alert.showAndWait();
                    nuevaVentana.close();
                }

            }
        });
        Button cancelar = new Button("Cancelar");
        cancelar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuevaVentana.close();
            }
        });
        panelBotones.getChildren().addAll(aceptar, cancelar);
        panelVentana.setBottom(panelBotones);
        panelVentana.setCenter(grid);
        Scene scene = new Scene(panelVentana, 400, 400);
        nuevaVentana.setScene(scene);
        nuevaVentana.show();
    }

    public void ventantaEliminarParadero(int idRuta) {
        ConsultaParadero consultaParadero = new ConsultaParadero(getConexion());
        if (consultaParadero.eliminarParadero(idRuta)) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Información");
            alert.setHeaderText(null);
            alert.setContentText("Datos eliminados exitosamente!");
            alert.showAndWait();

            btnRuta.fire();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("Datos NO se eliminaron");
            alert.showAndWait();
        }

    }

    public BorderPane getVistaRutas() {
        return this;
    }
}
