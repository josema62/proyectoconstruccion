/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import ModeloDatos.Paradero;
import ModeloDatos.SalidaAux;
import conexion.ConexionBD;
import conexion.ConsultaHora;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Usuario
 */
public class ControladorHorario {
    
    private Connection con;
    private SimpleDateFormat sdf;
    private ArrayList<String> horarios;
    private ArrayList<Paradero> paraderos;
    private SalidaAux salida;
    
    public ControladorHorario(ConexionBD conexionBD,ArrayList<String> horarios, ArrayList<Paradero> paraderos, SalidaAux salida){
        
        this.con = conexionBD.obtenerConexion();
        
        this.sdf = new SimpleDateFormat("HH:mm:ss");
        
        this.horarios = horarios;
        this.paraderos = paraderos;
        this.salida = salida;
        
    }
    
    public void actualizarHorariosParaderos(int retraso, int i){ // actualizar desde el horario i hacia adelante
        int n = horarios.size();
        ConsultaHora consultaHora = new ConsultaHora(con);
        
        if (i == 0){
            String horario = salida.getFechaSalida();
            String nuevoHorario = sumarRetraso(horario, retraso);
            salida.setFechaSalida(nuevoHorario);
            
            horario = salida.getFechaEntrada();
            nuevoHorario = sumarRetraso(horario, retraso);
            salida.setFechaEntrada(nuevoHorario);
        }
        
        if (i > 0){
            String horario = salida.getFechaEntrada();
            String nuevoHorario = sumarRetraso(horario, retraso);
            salida.setFechaEntrada(nuevoHorario);
            i = i - 1;
        }
        /*
        if (i == 1){
            i = i - 1;
        }
                */
        for (int h = i; h < n; h++) {
            Paradero paradero = paraderos.get(h);
            int idParadero = Integer.valueOf(paradero.getId());
            String horario = horarios.get(h);
            String nuevoHorario = sumarRetraso(horario, retraso);
            // colocar consultaHora
            consultaHora.updateHoraParadero(idParadero, nuevoHorario);
            
            horarios.set(h, nuevoHorario);
        }
    }
    
    
    private String sumarRetraso(String hora,int retraso){
        Date date = null;
        try {
            date = sdf.parse(hora);
        } catch (ParseException ex) {
            Logger.getLogger(ControladorHorario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, retraso);
        
        String nuevoHorario = sdf.format(calendar.getTime());
        
        return nuevoHorario;
    }
    
}
